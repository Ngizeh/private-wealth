<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
	use Imageable;

	protected $fillable = [
		'path', 'name','thumbnail_path'
	];

	protected $baseDir = 'properties/photos';
}
