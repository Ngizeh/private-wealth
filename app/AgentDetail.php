<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentDetail extends Model
{
	protected $fillable = [
		'contact_person',
		'web_address',
		'email',
		'phone_number'
	];
}
