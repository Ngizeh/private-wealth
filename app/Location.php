<?php

namespace App;

use App\Property;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
     'address','state','country', 'zip_code','lat','lng'
    ];
    
    public function property()
    {
       return $this->belongsTo(Property::class);     
    }

    public function scopePublished($query)
     {
         return $query->whereNotNull('published_at');
     }
}
