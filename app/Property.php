<?php

namespace App;

use App\Amenity;
use App\Media;
use App\User;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Property extends Model
{
    use Sluggable;

    protected $fillable = [
        'title',
        'type',
        'status',
        'location',
        'price',
        'description',
        'bedroom',
        'bathroom',
        'garage',
        'area',
        'plot_size',
        'parking',
        'price_prefix',
        'price_label',
        'listed_for',
        'property_identity',
        'year_built',
        'published_at'
    ];

     public function getRouteKeyName()
    {
        return 'slug';
    }

    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function scopeSearch($query, $search)
    {
     return $query->where('location', 'LIKE', "%$search%")
                 ->orWhere('type', 'LIKE', "%$search%")
                 ->orWhere('status', 'LIKE', "%$search%")
                ->orWhere('price', 'LIKE', "%$search%");
    }
    

     public function scopePublished($query)
     {
         return $query->whereNotNull('published_at');
     }
     public function scopeSale($query)
     {
         return $query->whereStatus('for sale');
     }
     public function scopeResidential($query)
     {
         return $query->whereListedFor('residential');
     }
     public function scopeCommercial($query)
     {
         return $query->whereListedFor('commercial');
     }
     public function scopeRental($query)
     {
         return $query->whereStatus('rental');
     }

    public function user()
    {
       return $this->belongsTo(User::class);
    }

    public static function locatedAt($slug)
    {
        return static::where(compact('slug'))->firstOrFail();
    }

    public function amenities()
    {
       return $this->hasMany(Amenity::class);
    }
    public function locating()
    {
       return $this->hasMany(Location::class);
    }
    public function path()
    {
       return 'amenity/'.$this->slug;
    }


    public function medias()
    {
       return $this->hasMany(Media::class);
    }

     public function addPhoto(Media $media)
    {
        return $this->medias()->save($media);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('M d, y');
    }

    public function getShorTitleAttribute()
    {
    	return Str::limit($this->title, 15);
    }

    public function getShortDescriptionAttribute()
    {
        return Str::limit($this->description, 150);
    }


    public function getExcerptAttribute()
    {
    	return Str::limit($this->description, 27);
    }


    public function deleteFileAndData()
    {
        $this->medias->each(function($media)  {
            \File::delete([
                public_path($media->path),
                public_path($media->thumbnail_path)
            ]);
        });
        $this->delete();

        return $this;
    }


}
