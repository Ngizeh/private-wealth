<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandAmenity extends Model
{
    protected $fillable = [
      'fencing',
      'water',
      'electricity',
      'access_road',
      'existing_building',
      'land_id'
    ];
}
