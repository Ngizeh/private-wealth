<?php

namespace App;

use App\Property;
use Illuminate\Database\Eloquent\Model;

class Amenity extends Model
{
    protected $fillable = [
    "gym",
    "swimming",
    "cctv",
    "scenic_view",
    "solar_panel",
    "bio_digestor",
    "underground",
    "internet",
    "lift_elevator",
    "pets_allowed",
    "balcony",
    "alarm_backup",
    "wifi",
    "bore_hole",
    "electric_fence",
    "garden",
    "build_in_cupboard",
    "ensuite",
    "water_tank",
    "walkin_closet",
    "property_id"
    ];
    
    public function property()
    {
       return $this->belongsTo(Property::class, 'property_id');     
    }
}
