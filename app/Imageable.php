<?php

namespace App;

use Intervention\Image\Facades\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

trait Imageable {

	public static function named($name)
	{
		return (new static)->saveAs($name);
	}

	public function saveAs($name)
	{
		$this->name = sprintf("%s-%s", time(), $name);
		$this->path = sprintf("%s/%s", $this->baseDir, $this->name);
		$this->thumbnail_path = sprintf("%s/tn-%s", $this->baseDir, $this->name);

		return $this;

	}

	public function move(UploadedFile $file)
	{
		$file->move($this->baseDir, $this->name);

		$this->makeThumbnail();
		$this->resizeImage();

		return $this;
	}

	protected function makeThumbnail()
	{
		Image::make($this->path)->fit(350, 221)->save($this->thumbnail_path);
	}

	protected function resizeImage()
	{
		Image::make($this->path)->fit(1920, 650)->save($this->path);
	}

	public function delete()
	{
		\File::delete([
			$this->name,
			$this->path,
			$this->thumbnail_path
		]);

		parent::delete();
	}
}




?>