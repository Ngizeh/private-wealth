<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AmenityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        "gym" => "string|nullable",
        "swimming" => "string|nullable",
        "cctv" => "string|nullable",
        "scenic_view" => "string|nullable",
        "solar_panel" => "string|nullable",
        "bio_digestor" => "string|nullable",
        "underground" => "string|nullable",
        "internet" => "string|nullable",
        "lift_elevator" => "string|nullable",
        "pets_allowed" => "string|nullable",
        "balcony" => "string|nullable",
        "alarm_backup" => "string|nullable",
        "wifi" => "string|nullable",
        "bore_hole" => "string|nullable",
        "electric_fence" => "string|nullable",
        "garden" => "string|nullable",
        "build_in_cupboard" => "string|nullable",
        "ensuite" => "string|nullable",
        "water_tank" => "string|nullable",
        "walkin_closet" => "string|nullable"
    ];
    }
}
