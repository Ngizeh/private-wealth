<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LandAmenityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'fencing' => 'string|nullable',
          'water' => 'string|nullable',
          'electricity' => 'string|nullable',
          'access_road' => 'string|nullable',
          'existing_building' => 'string|nullable',
        ];
    }
}
