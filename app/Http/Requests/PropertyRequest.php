<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
       'title' => 'required',
        'type' => 'required',
        'status' => 'required',
        'location' => 'required',
        'price' => 'required', 
        'description' => 'required',
        'listed_for' => 'required',
        'bedroom' => 'integer|nullable', 
        'bathroom' => 'integer|nullable', 
        'garage' => 'integer|nullable', 
        'area' => 'integer|nullable', 
        'plot_size' => 'integer|nullable', 
        'parking' => 'integer|nullable', 
        'price_prefix' => 'integer|nullable', 
        'price_label' => 'string|nullable', 
        'property_identity' => 'string|nullable',
        'year_built' => 'integer|nullable',
        'publish' => 'integer'
        ];
    }
}
