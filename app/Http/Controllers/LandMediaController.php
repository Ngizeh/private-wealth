<?php

namespace App\Http\Controllers;

use App\Land;
use App\LandMedia;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LandMediaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function create(Land $Land, $slug)
    {
        $land = Land::locatedAt($slug);

        return view('lands.land_media',compact('land', 'slug'));
    }

    public function store(Request $request,$slug)
    {
       $this->validate($request, ['file' => 'required|mimes:jpg,jpeg,png,bmp']);

        $photo = $this->makePhoto($request->file('file'));

        Land::locatedAt($slug)->addPhoto($photo);

    }

    protected function makePhoto(UploadedFile $file)
    {
        return LandMedia::named($file->getClientOriginalName())->move($file);
    }


   public function destroy($id)
    {
        LandMedia::findOrFail($id)->delete();

        return back();

    }
}
