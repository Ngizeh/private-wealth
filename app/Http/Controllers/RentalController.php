<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

class RentalController extends Controller
{
    public function index(Property $property)
    {
        $properties = Property::rental()
            ->with('medias')->orderBy('created_at', 'desc')->paginate(9);
       return view('property.rental.index', compact('properties'));     
    }
}
