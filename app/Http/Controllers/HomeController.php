<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(Property $property)
    {
       $properties = Property::published()->orderBy('created_at', 'desc')->with('medias')
                     ->orderBy('created_at', 'desc')->paginate(12);

        return view('home', compact('properties'));
    }
}
