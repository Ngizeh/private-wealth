<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function index(Property $property)
    {
       $properties = Property::sale()
            ->with('medias')->orderBy('created_at', 'desc')->paginate(9);
       return view('property.sale.index', compact('properties'));  
    }
}
