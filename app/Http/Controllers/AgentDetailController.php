<?php

namespace App\Http\Controllers;

use App\AgentDetail;
use App\Http\Requests\AgentDetailRequest;
use Illuminate\Http\Request;

class AgentDetailController extends Controller
{

    public function create()
    {
         return view('property.create.agent');
    }


    public function store(AgentDetailRequest $request, AgentDetail $agent)
    {
        $agent = new AgentDetail($request->all());
        $agent->save();
        return redirect('media/create');
    }

}
