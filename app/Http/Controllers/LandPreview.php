<?php

namespace App\Http\Controllers;

use App\Land;
use Illuminate\Http\Request;

class LandPreview extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function show(Land $land, $slug)
    {
       $land = Land::locatedAt($slug)->load('landMedias','landAmenities');

        return view('lands.land_preview', compact('land'));
    }

   public function update(Request $request, Land $land, $slug)
    {
        $land = Land::locatedAt($slug);

        $land->update($request->only('published_at'));

        return redirect('/lands-property');
    }
}
