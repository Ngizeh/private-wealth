<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

class ResidentialsController extends Controller
{
    public function index(Property $property)
    {
        $properties = Property::residential()
            ->with('medias')->orderBy('created_at', 'desc')->paginate(9);
       return view('property.residential.index', compact('properties'));  
    }
}
