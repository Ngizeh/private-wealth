<?php

namespace App\Http\Controllers;

use App\Land;
use App\LandAmenity;
use Illuminate\Http\Request;
use App\Http\Requests\LandAmenityRequest;

class LandAmenityController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }
    public function create(LandAmenity $land_amenity, Land $land, $slug)
    {
        $land = Land::locatedAt($slug);

         return view('lands/land_amenities', compact('land','land_amenity'));
    }

     public function edit($slug, Request $request)
    {
        $land = Land::locatedAt($slug);

        foreach ($land->landAmenities as $land_amenity)

        return view('lands.amenities_edit', compact('land_amenity','land','slug'));
    }

   public function store(LandAmenityRequest $request, Land $Land, $slug)
    {
        $amenity = new LandAmenity($request->all());
        Land::locatedAt($slug)->landAmenities()->save($amenity);
        return redirect('/land-media/'.$slug);
    }

    public function update(Request $request, LandAmenity $land_amenity,  $slug)
    {
        $land = Land::locatedAt($slug);

        foreach ($land->landAmenities as $amenity)

        $amenity->update($request->except('_method','_token'));

         return redirect('land-media/'.$slug);
    }

}
