<?php

namespace App\Http\Controllers;

use App\Land;
use App\Property;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $properties = Property::latest()->paginate(12);
        return view('user-dashboard.properties_table', compact('properties'));
    }

    public function show()
    {
        return view('user-dashboard.settings');
    }

    public function lands()
    {
        $lands = Land::latest()->paginate(15);
        return view('user-dashboard.lands_table', compact('lands'));
    }

    public function update(Request  $request, User $user)
    {
        $data = $request->validate([
            'name' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ]);

       $user->update([
           'name' => $data['name'],
           'password' => Hash::make($data['password'])
       ]);

       return redirect()->back()->with('update', 'Successfully Updated the Your Settings');
    }
}
