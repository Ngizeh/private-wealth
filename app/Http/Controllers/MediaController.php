<?php

namespace App\Http\Controllers;

use App\Media;
use App\Property;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('property.create.media');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Property $property, $slug)
    {
        $property = Property::locatedAt($slug);
        return view('property.create.media',compact('property', 'slug'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$slug)
    {
       $this->validate($request, ['file' => 'required|mimes:jpg,jpeg,png,bmp']);

        $photo = $this->makePhoto($request->file('file'));
        
        Property::locatedAt($slug)->addPhoto($photo);

    }

    protected function makePhoto(UploadedFile $file)
    {
        return Media::named($file->getClientOriginalName())->move($file);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Media  $media
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
    {
        Media::findOrFail($id)->delete();

        return back();

    }
}
