<?php

namespace App\Http\Controllers;

use App\Http\Requests\LocationRequest;
use App\Http\Requests\PropertyRequest;
use App\Location;
use App\Property;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    
     public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Property $property, $slug)
    {
        $property = Property::locatedAt($slug);
        return view('property.create.location',compact('property', 'slug'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Property $property, $slug)
    {
        $location = new Location($request->all());
        Property::locatedAt($slug)->locating()->save($location);
        return redirect('/media/'.$slug);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
      public function edit(Property $property, $slug, Request $request)
    {
        $property = Property::locatedAt($slug)->load('location');

        return view('property.edit.location', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
     public function update(LocationRequest $request, Property $property)
    {
        $property->update($request->all());
        
        $slug = str_slug(request('title'));

        return redirect('media/'.$slug.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        //
    }
}
