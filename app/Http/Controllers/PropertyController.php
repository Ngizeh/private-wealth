<?php

namespace App\Http\Controllers;

use App\Http\Requests\PropertyRequest;
use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PropertyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index(Property $property)
    {
        $properties = Property::published()->with('medias')->latest()->paginate(15);
        return view('property.index', compact('properties'));
    }


    public function create(Property $property)
    {
        return view('property.create.description', compact('property'));
    }


    public function store(PropertyRequest $request, Property $property)
    {
        $property = new Property($request->all());

        $property['property_identity'] = substr('PWRE'.md5(mt_rand()), 0,7);

        Auth::user()->properties()->save($property);

        return redirect($property->path());
    }


    public function show(Property $property, $slug)
    {
       $property = Property::locatedAt($slug)->load('medias','amenities');

        return view('property.show', compact('property'));
    }


    public function edit($slug, Request $request)
    {
        $property = Property::locatedAt($slug)->load('medias','amenities');

        return view('property.edit.description', compact('property'));
    }


    public function update(PropertyRequest $request, Property $property, $slug)
    {

       $property = Property::locatedAt($slug);

        $property->update($request->all());

        return redirect('amenity/'.$slug.'/edit');
    }


    public function destroy(Property $slug)
    {
        $slug->deleteFileAndData();

        return back();

    }
}
