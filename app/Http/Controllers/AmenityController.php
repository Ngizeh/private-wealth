<?php

namespace App\Http\Controllers;

use App\Amenity;
use App\Property;
use Illuminate\Http\Request;
use App\Http\Requests\AmenityRequest;

class AmenityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        return view('amenity.show');
    }

    public function create(Amenity $amenity, Property $property, $slug)
    {
        $property = Property::locatedAt($slug);

        return view('property.create.amenities', compact('amenity','property','slug'));
    }


    public function store(AmenityRequest $request, Property $property, $slug)
    {
        $amenity = new Amenity($request->all());

        Property::locatedAt($slug)->amenities()->save($amenity);

        return redirect('/media/'.$slug);
    }


    public function edit($slug, Request $request)
    {
        $property = Property::locatedAt($slug);

        foreach ($property->amenities as $amenity)

        return view('property.edit.amenity', compact('amenity','property','slug'));
    }

    public function update(Request $request, Amenity $amenity,  $slug)
    {
        $property = Property::locatedAt($slug);

        foreach ($property->amenities as $amenity)


        $amenity->update($request->except('_method','_token'));

         return redirect('media/'.$slug);
    }

}
