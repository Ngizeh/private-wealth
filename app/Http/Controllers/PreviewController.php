<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PropertyRequest;

class PreviewController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');    
    }
    public function show(Property $property, $slug)
    {
       $property = Property::locatedAt($slug)->load('medias','amenities','locating');
       
        return view('property.preview', compact('property'));
    }
   public function update(Request $request, Property $property, $slug)
    {
       $property = Property::locatedAt($slug);

       $property->update($request->only('published_at'));

        return redirect('/');
    }
  
}
