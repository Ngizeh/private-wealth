<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

class CommercialsController extends Controller
{
     public function index(Property $property)
    {
       $properties = Property::where('listed_for', '=', 'Commercial')
            ->with('medias')->orderBy('created_at', 'desc')->paginate(9);
       return view('property.commercial.index', compact('properties'));
    }
}
