<?php

namespace App\Http\Controllers;

use App\Http\Requests\LandRequest;
use App\Land;
use App\LandMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

use SebastianBergmann\CodeCoverage\Report\Xml\Project;

class LandsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index(Land $land)
    {
        $properties = Land::published()->with('landMedias')->latest()->paginate(9);

        return view('lands.index', compact('properties'));
    }

    public function create(Land $land)
    {
        return view('lands.create', compact('land'));
    }

    public function store(LandRequest $request, Land $land)
    {
        $land = new Land($request->all());

        $land['property_identity'] = substr('PWRE' . md5(mt_rand()), 0, 7);

        auth()->user()->lands()->save($land);

        return redirect($land->path());
    }


    public function show(Land $land, $slug)
    {
        $property = Land::locatedAt($slug)->load('landMedias', 'landAmenities');

        return view('lands.show', compact('property'));
    }


    public function edit($slug, Request $request)
    {
        $land = Land::locatedAt($slug)->load('landMedias', 'landAmenities');

        return view('lands.edit', compact('land'));
    }


    public function update(LandRequest $request, Land $land, $slug)
    {

        $land = Land::locatedAt($slug);

        $land->update($request->all());

        return redirect('land-amenities/' . $slug . '/edit');
    }

    public function destroy(Land $slug)
    {
        $slug->deleteFileAndData();

        return back();
    }
}
