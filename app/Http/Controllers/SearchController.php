<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Property $property, Request $request)
    {
        $query = $request->input('t','p', 's', 'h');

        $properties = $query ? Property::search($query)->orderBy('created_at', 'desc')->paginate(12)
                   : Property::paginate(12);

       return view('search', compact('properties'));
    }
        
}
