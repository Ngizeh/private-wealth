<?php

namespace App\Utilities;

class AvailableTitle {

    protected static $title = ["Yes", "No"];

    public static function all()
    {
        return array_values(static::$title);
    }
}
