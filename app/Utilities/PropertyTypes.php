<?php
namespace App\Utilities;

class PropertyTypes {
    
    protected static $types = [
        "Apartment",
        "Ambassadorial Homes",
        "Bungalow",
        "Duplex",
        "Houses",
        "Mansionate",
        "Penthouse",
        "Villas",
        "Town Houses",
        "Studio",
        "Office Space",
        "Commercial Retail",
        "Go Down", 
        "Standalones" ,
        "Bedsitter",
        "Guest House"
    ];
    
    public static function all()
    {
        return array_values(static::$types);     
   }
}
