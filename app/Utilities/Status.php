<?php
namespace App\Utilities;

class Status {
    
   protected static $status = [
        "rental",
        "for sale",
];

  public static function all()
  {
     return array_values(static::$status);     
  }
}
