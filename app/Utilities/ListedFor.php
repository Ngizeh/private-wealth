<?php
namespace App\Utilities;

class ListedFor {
    
   protected static $listed = [
        "commercial",
        "residential",
];

  public static function all()
  {
     return array_values(static::$listed);     
  }
}

?>
