<?php

namespace App;

use App\LandAmenity;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Land extends Model
{
    use Sluggable;

    protected $fillable = [
        'location',
        'size',
        'title',
        'price',
        'description',
        'published_at'
    ];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function scopePublished($query)
     {
         return $query->whereNotNull('published_at');
     }

    public function sluggable() :array
    {
        return [
            'slug' => [
                'source' => 'location'
            ]
        ];
    }

    public function path()
    {
       return '/land-amenities/'.$this->slug;
    }

    public function user()
    {
       return $this->belongsTo(User::class);
    }

    public static function locatedAt($slug)
    {
        return static::where(compact('slug'))->firstOrFail();
    }

    public function landAmenities()
    {
       return $this->hasMany(LandAmenity::class);
    }
    public function landMedias()
    {
       return $this->hasMany(LandMedia::class);
    }
    public function addPhoto(LandMedia $media)
    {
        return $this->landMedias()->save($media);
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('M d, y');
    }

    public function deleteFileAndData()
    {
       $this->landMedias->each(function($media) {
            \File::delete([
                public_path($media->path),
                public_path($media->thumbnail_path),
            ]);
        });

       $this->delete();

       return $this;
    }
}
