<?php

namespace App;

use Intervention\Image\Facades\Image;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LandMedia extends Model
{
	use Imageable;

	protected $fillable = [
		'path', 'name','thumbnail_path'
	];

	protected $baseDir = 'lands/photos';

	public function land()
	{
		return $this->belongsTo(Land::class);
	}
}
