@extends('layouts.admin')

@section('content')
    <div x-init="$el.focus()" class="flex-1 overflow-auto focus:outline-none" tabindex="0">
        @include('includes.user-dashboard', ['title' =>'Our Lands'])

        <main class="flex-1 relative pb-8 z-0 overflow-y-auto">
            <!--- Lands Table -->
            <div class="mt-8">

                <h2 class="max-w-6xl mx-auto my-8 px-4 text-lg leading-6 font-medium text-gray-900 sm:px-6 lg:px-8">
                    Recent activity on Lands
                </h2>

                <!-- Activity list (smallest break point only) -->
                <div class="shadow sm:hidden">
                    <ul class="mt-2 divide-y divide-gray-200 overflow-hidden shadow sm:hidden" x-max="1">
                        @foreach($lands as $land)
                            <li>
                                <a href="/land/{{ $land->slug }}" class="block px-4 py-4 bg-white hover:bg-gray-50">
                                    <span class="flex items-center space-x-4">
                                        <span class="flex-1 flex space-x-2 truncate">
                                            <svg class="flex-shrink-0 h-5 w-5 text-gray-400" x-description="Heroicon name: cash" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                <path fill-rule="evenodd" d="M4 4a2 2 0 00-2 2v4a2 2 0 002 2V6h10a2 2 0 00-2-2H4zm2 6a2 2 0 012-2h8a2 2 0 012 2v4a2 2 0 01-2 2H8a2 2 0 01-2-2v-4zm6 4a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd"></path>
                                            </svg>
                                            <span class="flex flex-col text-gray-500 text-sm truncate">
                                                <span class="truncate">{{ $land->title }}</span>
                                                <span class="text-gray-900 font-medium">Ksh. {{ number_format($land->price) }}</spa>
                                                    <span>{{ $land->created_at }}</span>
                                            </span>
                                        </span>
                                        <svg class="flex-shrink-0 h-5 w-5 text-gray-400" x-description="Heroicon name: chevron-right" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
                                        </svg>
                                    </span>
                                </a>
                            </li>
                        @endforeach
                    </ul>

                    <nav class="bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200" aria-label="Pagination">
                        <div class="flex-1 flex justify-between">
                            <a href="#" class="relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:text-gray-500">
                                Previous
                            </a>
                            <a href="#" class="ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:text-gray-500">
                                Next
                            </a>
                        </div>
                    </nav>
                </div>

                <!-- Activity table (small break point and up) -->
                <div class="hidden sm:block">
                    <div class="max-w-6xl mx-auto px-4 sm:px-6 lg:px-8">
                        <div class="flex flex-col mt-2">
                            <div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
                                <table class="min-w-full divide-y divide-gray-200">
                                    <thead>
                                    <tr>
                                        <th class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Ref No
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50 text-center text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Title Available
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Location
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Price
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            created on
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Edit
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            View
                                        </th>
                                        <th class="px-6 py-3 bg-gray-50 text-right text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Delete
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="bg-white divide-y divide-gray-200" x-max="1">
                                    @forelse($lands as $land)
                                        <tr class="bg-white">
                                            <td class="px-6 py-4 text-left whitespace-nowrap text-sm text-gray-500">
                                                {{$land->property_identity}}
                                            </td>
                                            <td class="px-6 py-4 text-center whitespace-nowrap text-sm text-gray-500">
                                                {{$land->title ? 'Yes' : 'No'}}
                                            </td>
                                            <td class="px-6 py-4 text-center whitespace-nowrap text-sm text-gray-500">
                                                {{$land->location}}
                                            </td>
                                            <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
                                                <span class="text-gray-900 font-medium"> {{number_format($land->price)}}</span>
                                            </td>
                                            <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-gray-500">
                                                {{$land->created_at}}
                                            </td>
                                            <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-cyan-800">
                                                <a href="/lands-property/{{$land->slug}}">View</a>
                                            </td>
                                            <td class="px-6 py-4 text-right whitespace-nowrap text-sm text-cyan-800">
                                                <a href="/lands-property/{{$land->slug}}/edit" >Edit</a>
                                            </td>
                                            <td class="hidden px-6 text-right py-4 whitespace-nowrap text-sm text-gray-500 md:block">
                                                <form method="post" action="/lands-property/{{$land->slug}}">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit"
                                                            class="inline-flex px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">
                                                        Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr class="bg-white">
                                            <td colspan="8" class="w-full px-6 py-4 text-left whitespace-nowrap text-sm text-gray-500 text-center">
                                                <p class="text-lg">No Lands Uploaded yet</p>
                                            </td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            <!-- Pagination -->
                                {{ $lands->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection
