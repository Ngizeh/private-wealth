@extends('layouts.admin')

@section('content')
<div >
    <div class="flex" style="height: 700px">
        <div class="w-1/6 bg-blue-darker text-white">
           <h4 class="bg-blue p-3 text-center">Admin Dashboard</h4>
             <p class="px-4 py-4 text-sm text-grey-darker border-b border-grey-light">
                 <img  class="h-10 w-10 rounded-full border-2 border-white"src="{{asset('images/index.jpeg')}}" alt="">
                 <span class="px-2">Admin Name</span>
             </p>
           <div class="flex-col">
            <div class="flex-1">
               <ul class="list-reset p-4">
                <li class="mx-3 py-2 px-2">
                    <a class="flex items-center no-underline hover:text-white" class=" flex items-center no-underline hover:text-white" href="">
                     <svg class="fill-current text-grey-grey-light h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                        <path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm-5.6-4.29a9.95 9.95 0 0 1 11.2 0 8 8 0 1 0-11.2 0zm6.12-7.64l3.02-3.02 1.41 1.41-3.02 3.02a2 2 0 1 1-1.41-1.41z"/>
                    </svg>
                     <span class="pl-2">Dashboard</span>
                  </a>
                </li>
                <li class="mx-3 py-2 px-2">
                    <a class="flex items-center no-underline hover:text-white" href="">
                     <svg class="fill-current text-grey-grey-light h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M7 8a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0 1c2.15 0 4.2.4 6.1 1.09L12 16h-1.25L10 20H4l-.75-4H2L.9 10.09A17.93 17.93 0 0 1 7 9zm8.31.17c1.32.18 2.59.48 3.8.92L18 16h-1.25L16 20h-3.96l.37-2h1.25l1.65-8.83zM13 0a4 4 0 1 1-1.33 7.76 5.96 5.96 0 0 0 0-7.52C12.1.1 12.53 0 13 0z"/></svg>
                    <span class="pl-2">Users</span>
                </a>
                </li>
                <li class="mx-3 py-2 px-2">
                    <a class="flex items-center no-underline hover:text-white" href="">
                        <svg class="fill-current text-grey-grey-light h-5 w-5"
                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M8 20H3V10H0L10 0l10 10h-3v10h-5v-6H8v6z"/></svg>
                    <span class="pl-2">Properties</span>
                </a>
                </li>
                <li class="mx-3 py-2 px-2">
                    <a class="flex items-center no-underline hover:text-white" href="">
                    <svg  class="fill-current text-grey-grey-light h-5 w-5"
                    xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M4.13 12H4a2 2 0 1 0 1.8 1.11L7.86 10a2.03 2.03 0 0 0 .65-.07l1.55 1.55a2 2 0 1 0 3.72-.37L15.87 8H16a2 2 0 1 0-1.8-1.11L12.14 10a2.03 2.03 0 0 0-.65.07L9.93 8.52a2 2 0 1 0-3.72.37L4.13 12zM0 4c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4z"/></svg>
                    <span class="pl-2">Stats</span>
                </a>
                </li>
                <li class="mx-3 py-2 px-2">
                    <a class="flex items-center no-underline hover:text-white" href="">
                        <svg class="fill-current text-grey-grey-light h-5 w-5"
                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M1 4c0-1.1.9-2 2-2h14a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V4zm2 2v12h14V6H3zm2-6h2v2H5V0zm8 0h2v2h-2V0zM5 9h2v2H5V9zm0 4h2v2H5v-2zm4-4h2v2H9V9zm0 4h2v2H9v-2zm4-4h2v2h-2V9zm0 4h2v2h-2v-2z"/></svg>
                    <span class="pl-2">Something Else</span>
                </a>
                </li>
                <li class="mx-3 py-2 px-2">
                        <a  class="flex items-center no-underline hover:text-white" href="">
                         <svg  class="fill-current text-grey-grey-light h-5 w-5"
                         xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 0h6v6H0V0zm2 2v2h2V2H2zm12-2h6v6h-6V0zm2 2v2h2V2h-2zm-2 12h6v6h-6v-6zm2 2v2h2v-2h-2zM0 14h6v6H0v-6zm2 2v2h2v-2H2zM6 2h8v2H6V2zm0 14h8v2H6v-2zM16 6h2v8h-2V6zM2 6h2v8H2V6zm5 1h6v2H7V7zm2 2h2v4H9V9z"/></svg>
                        <span class="pl-2">Tables</span>
                        </a>
                    </li>
                        </ul>
           </div>
           <div>
               <ul class="list-reset">
                   <li class="mx-3 py-2 px-2">
                       <a class="flex items-center no-underline hover:text-white" href="">
                        <svg  class="fill-current text-grey-light h-5 w-5 border-2 border-grey-dark rounded-full"
                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 8.586L2.929 1.515 1.515 2.929 8.586 10l-7.071 7.071 1.414 1.414L10 11.414l7.071 7.071 1.414-1.414L11.414 10l7.071-7.071-1.414-1.414L10 8.586z"/></svg>
                           <span class="pl-2">Sign Out</span>
                       </a>
                   </li>
               </ul>
           </div>
       </div>
        </div>
        <div class="w-5/6 bg-grey-light">
         <div class="container mx-3 px-6">
             <h3>Main Content</h3>
             <p class="py-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur iure sunt dolor sequi, tempore ipsa modi harum! Possimus accusamus labore alias ut, earum quisquam pariatur, sint nobis quo numquam corporis.</p>
             <p class="py-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, quaerat praesentium odit iure nobis optio minus, provident mollitia quasi qui deleniti autem nisi. Ipsam, nostrum, ex. Eos adipisci, fugiat quasi.</p>
             <p class="py-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero optio amet molestias explicabo dignissimos nesciunt repellat, vitae, minima quod magni a distinctio magnam necessitatibus consequuntur nisi, doloremque, totam animi hic.</p>
             <p class="py-6">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium libero iure doloremque odio vitae unde, assumenda eius iusto, delectus repellat ratione officiis maxime debitis dolor quidem. Necessitatibus totam consectetur quis.</p>
         </div>
        </div>
    </div>
</div>
@endsection
