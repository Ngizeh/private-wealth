@extends('layouts.app')

@section('content')

    <main>
        <section class="pri-pad">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-9 left-block">
                        <div class="sec-title icon-wrap">
                            <h3>Commercial Propeties</h3>
                        </div>
                        <!--title-->

                        @include('property.partial')
                </div>
            </div>
        </section>
        <!--property wrap-->
    </main>

@endsection

