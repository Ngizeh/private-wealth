@extends('layouts.app')

@section('content')

    <main>
        <div class="filter-bar filter-bar-2 primary-bg text-white">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <form action="#" class="form-inline">
                            <div class="form-group">
                                <input type="text" placeholder="Enter Keywords">
                            </div>
                            <!--keyword-->

                            <div class="form-group select">
                                <label>Location</label>
                                <select>
                                    <option>Nairobi</option>
                                    <option>Mombasa</option>
                                    <option>Kisumu</option>
                                    <option>Kakamega</option>
                                </select>
                            </div>
                            <!--location-->

                            <div class="form-group select">
                                <label>property type</label>
                                <select>
                                    <option>Apartment</option>
                                    <option>House</option>
                                </select>
                            </div>
                            <!--type-->

                            <div class="form-group select">
                                <label>Min Price</label>
                                <select>
                                    <option>any Price</option>
                                </select>
                            </div>
                            <!-- min price-->

                            <div class="form-group select">
                                <label>Max Price</label>
                                <select>
                                    <option>any Price</option>
                                </select>
                            </div>
                            <!--max price-->

                            <div class="form-group">
                                <button type="submit">
                                    Start Search <i class="fa fa-long-arrow-right"></i>
                                </button>
                            </div>
                            <!--max price-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--filter bar-->

        <div class="gallery-wrap full">
            <div class="banner-content">
                <div class="container">
                 <span class="tag left text-uppercase primary-bg text-white">
                    {{$property->status}}</span>
                    <div class="title text-white">
                        <h2 class="mb-0 lh-normal text-white">
                            {{$property->title}}
                        </h2> {{$property->location}}
                        <span class="price primary-bg text-white">Ksh. {{number_format($property->price)}}</span>
                    </div>
                </div>
            </div>

            <ul class="gallery-full">
                @foreach($property->medias as $set)
                    <li>
                        <figure class="text-center overlay overlay2 resize-image">
                            <img src="/{{$set->path}}" alt="{{ $property->name }}">
                        </figure>
                    </li>
                @endforeach
            </ul>
        </div>
        <!--gallery wrap-->

        <section class="pri-pad">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-9 property-single left-block ">
                        <div class="row mb-40">
                            <div class="col-sm-4 left-block ppt-info">
                                <div class="box border">
                                    <div class="box-title">
                                        <h6 class="mb-0 lh-normal">Property Informations</h6></div>
                                    <div class="detail info">
                                        <ul>
                                            <li>Property Identity <span>{{$property->property_identity}} </span></li>
                                            <li>Propery Type <span>{{$property->type}}</span> </li>
                                            <li>Bedrooms <span>{{$property->bedroom}}</span> </li>
                                            <li>Area <span>{{$property->area}}sq/ft </span></li>
                                            <li>Bathrooms <span>{{$property->bathroom}}</span> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--property-desc-->

                            <div class="col-sm-8 ppt-desc">
                                <div class="box border content-wrap mb-0">
                                    <h4 class="title mb-20">Property Descriptions</h4>
                                    <p>
                                        {{$property->description}}
                                    </p>

                                </div>
                            </div>
                            <!--property-desc-->
                        </div>
                        <!--property detail-->

                        <div class="box border mb-40">
                            <h4 class="title mb-20">Amenities</h4>
                            <ul class="amenities">
                                @foreach($property->amenities as $key => $amenity)
                                    <li>{!! $amenity->gym ? 'Gym' : '<del>Gym</del>' !!} </li>
                                    <li>{!! $amenity->swimming ? 'Swimming' : '<del>Swimming</del>' !!} </li>
                                    <li>{!! $amenity->cctv ? 'CCTV' : '<del>CCTV</del>' !!} </li>
                                    <li>{!! $amenity->scenic_view ? 'Scenic View' : '<del>Scenice View</del>' !!} </li>
                                    <li>{!! $amenity->garage ? 'Garage' : '<del>Garage</del>' !!} </li>
                                    <li>{!! $amenity->sauna_and_spa ? 'Sauna and Spa' : '<del>Sauna and Spa</del>' !!} </li>
                                    <li>{!! $amenity->golf_course ? 'Golf Course' : '<del>Golf Course</del>' !!} </li>
                                    <li>{!! $amenity->internet ? 'Internet' : '<del>Internet</del>' !!} </li>
                                    <li>{!! $amenity->lift_elevator ? 'Lift Elevator' : '<del>Lift Elevator</del>' !!} </li>
                                    <li>{!! $amenity->pets_allowed ? 'Pet Allowed' : '<del>Pet Allowed</del>' !!} </li>
                                    <li>{!! $amenity->balcony ? 'Balcony' : '<del>Balcony</del>' !!} </li>
                                    <li>{!! $amenity->alarm_backup ? 'Alarm' : '<del>Alarm</del>' !!} </li>
                                    <li>{!! $amenity->bedding ? 'Bedding' : '<del>Bedding</del>' !!} </li>
                                    <li>{!! $amenity->bore_hole ? 'Bore Hole' : '<del>Bore Hole</del>' !!} </li>
                                    <li>{!! $amenity->electric_fence ? 'Electric Fence' : '<del>Electric Fence</del>' !!} </li>
                                    <li>{!! $amenity->garden ? 'Garden' : '<del>Garden</del>' !!} </li>
                                    <li>{!! $amenity->build_in_cupboard ? 'Build in Cupboard' : '<del>Build in Cupboard</del>' !!} </li>
                                    <li>{!! $amenity->ensuite ? 'Ensuite' : '<del>Ensuite</del>' !!} </li>
                                    <li>{!! $amenity->kitchen ? 'Kitchen' : '<del>Kitchen</del>' !!} </li>
                                    <li>{!! $amenity->walkin_closet ? 'Walk in Closet' : '<del>Walk in Closet</del>' !!} </li>
                                @endforeach
                            </ul>
                        </div>
                        <!--aminities-->

                        <!--    <div class="box border mb-40">
                               <h4 class="title mb-20">Location Map</h4>
                                  <div class="map-hold">
                                    <div id="map-canvas" style="height:450px; width: 100%" class="form-control"></div>
                              </div>
                          </div> -->
                        <!--map hold-->

                        @include('includes.agent_details')
                        <!--agent detail-->
                    </div>
                    <!--left block-->

                    <div class="col-md-3 col-sm-3 sidebar">
                        <div class="alert-message sidebar-form mb-20 ">
                            <h6>Get daily property email alert in your inbox</h6>
                            <form action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Enter your email address">
                                </div>
                                <!--text-->

                                <div class="form-group">
                                    <button type="submit">
                                        create alert <i class="fa fa-long-arrow-right"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!--alert-->

                        <div class="sidebar-filter sidebar-form">
                            <h6 class="title">Find your home</h6>
                            <form action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Enter search keyword ">
                                </div>
                                <!--keyword-->

                                <div class="form-group select">
                                    <select>
                                        <option>Select Status </option>
                                    </select>
                                </div>
                                <!--status-->

                                <div class="form-group select">
                                    <select>
                                        <option>Select Location</option>
                                    </select>
                                </div>
                                <!--location-->

                                <div class="form-group select">
                                    <select>
                                        <option>Select Types </option>
                                    </select>
                                </div>
                                <!--type-->

                                <div class="form-group select">
                                    <select>
                                        <option>No. of Bedrooms</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                                <!--bed room-->

                                <div class="form-group select">
                                    <select>
                                        <option>No. of Bathrooms</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                                <!--bathroom-->


                                <div class="form-group range">
                                    <label>Price range</label>
                                    <div id="slider-range"></div>
                                    <input type="hidden" id="amount1">
                                    <input type="hidden" id="amount2">
                                    <div id="amount"></div>
                                </div>
                                <!--price-->

                                <div class="form-group range">
                                    <label>Area</label>
                                    <div id="area-range"></div>
                                    <input type="hidden" id="area1">
                                    <input type="hidden" id="area2">
                                    <div id="area"></div>
                                </div>
                                <!--area-->

                                <div class="form-group">
                                    <button type="submit">
                                        Start Search <i class="fa fa-long-arrow-right"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!--filter-->
                    </div>
                    <!--sidebar-->
                </div>
            </div>
        </section>
        <!--property wrap-->
    </main>

@endsection

