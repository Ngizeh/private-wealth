@extends('layouts.app')

@section('content')

<div class="container">
    <h3>Property Preview</h3>
    <div class="row">
        <p class="leading">
            Title : {{$property->title}}
            </<p>
            <hr>
            @foreach($property->medias->chunk(3) as $set)
            @foreach($set as $photo)
        <div class="col-md-4 col-lg-4" style="padding-bottom: 20px">
            <img src="/{{$photo->thumbnail_path}}" alt="">
        </div>
        @endforeach
        @endforeach
        <div class="col-md-12">
            <div class="box border mb-40">
                <h4 class="title mb-20">Descriptions</h4>
                <p>
                    {{$property->description}}
                </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box border mb-40">
                <h4 class="title mb-20">Property Information</h4>
                <div class="detail info">
                    <ul>
                        <li>Property Identity <span>{{$property->property_identity}} </span></li>
                        <li>Propery Type <span>{{$property->type}}</span> </li>
                        <li>Bedrooms <span>{{$property->bedroom}}</span> </li>
                        <li>Area <span>{{$property->area}}sq/ft </span></li>
                        <li>Bathrooms <span>{{$property->bathroom}}</span> </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box border mb-40">
                <h4 class="title mb-20">Amenities</h4>
                <ul class="amenities">
                    @foreach($property->amenities as $key => $amenity)
                    <li>{!! $amenity->gym ? 'Gym' : '<del>Gym</del>' !!} </li>
                    <li>{!! $amenity->swimming ? 'Swimming' : '<del>Swimming</del>' !!} </li>
                    <li>{!! $amenity->cctv ? 'CCTV' : '<del>CCTV</del>' !!} </li>
                    <li>{!! $amenity->scenic_view ? 'Scenic View' : '<del>Scenice View</del>' !!} </li>
                    <li>{!! $amenity->garage ? 'Garage' : '<del>Garage</del>' !!} </li>
                    <li>{!! $amenity->sauna_and_spa ? 'Sauna and Spa' : '<del>Sauna and Spa</del>' !!} </li>
                    <li>{!! $amenity->golf_course ? 'Golf Course' : '<del>Golf Course</del>' !!} </li>
                    <li>{!! $amenity->internet ? 'Internet' : '<del>Internet</del>' !!} </li>
                    <li>{!! $amenity->lift_elevator ? 'Lift Elevator' : '<del>Lift Elevator</del>' !!} </li>
                    <li>{!! $amenity->pets_allowed ? 'Pet Allowed' : '<del>Pet Allowed</del>' !!} </li>
                    <li>{!! $amenity->balcony ? 'Balcony' : '<del>Balcony</del>' !!} </li>
                    <li>{!! $amenity->alarm_backup ? 'Alarm' : '<del>Alarm</del>' !!} </li>
                    <li>{!! $amenity->bedding ? 'Bedding' : '<del>Bedding</del>' !!} </li>
                    <li>{!! $amenity->bore_hole ? 'Bore Hole' : '<del>Bore Hole</del>' !!} </li>
                    <li>{!! $amenity->electric_fence ? 'Electric Fence' : '<del>Electric Fence</del>' !!} </li>
                    <li>{!! $amenity->garden ? 'Garden' : '<del>Garden</del>' !!} </li>
                    <li>{!! $amenity->build_in_cupboard ? 'Build in Cupboard' : '<del>Build in Cupboard</del>' !!} </li>
                    <li>{!! $amenity->ensuite ? 'Ensuite' : '<del>Ensuite</del>' !!} </li>
                    <li>{!! $amenity->kitchen ? 'Kitchen' : '<del>Kitchen</del>' !!} </li>
                    <li>{!! $amenity->walkin_closet ? 'Walk in Closet' : '<del>Walk in Closet</del>' !!} </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <a href="/property/{{$property->slug}}/edit" class="btn btn-info">Edit</a>
        </div>
    </div>
    <div class="col-md-4">
        <form method="post" class="form-group" action="/preview/{{$property->slug}}">
            {{csrf_field()}}

            {{method_field('patch')}}
                <input type="hidden" name="published_at" value="{{ now() }}">

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Publish the Property</button>
            </div>
        </form>
    </div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

</div>


@endsection