@extends('layouts.app')

@section('content')

    <main class="pri-pad">
        <div class="container">
             @include('layouts.errors')
              <form method="post" class="property-submit" action="/property/{{$property->slug}}" >
                  @csrf
                  @method('patch')
                 @include('form_partials.description', ['submitButton' => 'Edit Description'])
              </form>

        </div>
    </main>

@endsection
