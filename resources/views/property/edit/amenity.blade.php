@extends('layouts.app')

@section('content')

    <main class="pri-pad">  
        <div class="container"> 
              <form method="post" class="property-submit" action="/amenities/{{$property->slug}}" >
                 {{method_field('patch')}}
                 @include('form_partials.amenities', [
                   'submitButton' => 'Edit Amenities'
                 ])
              </form>
        </div> 
    </main>

@endsection
