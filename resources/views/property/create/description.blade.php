@extends('layouts.app')

@section('content')

    <main class="pri-pad">  
        <div class="container"> 
            @include('property.create')
            @include('layouts.errors')
              <form method="post" class="property-submit" action="/property" >
                 @include('form_partials.description', [
                   'submitButton' => 'Create Description '
                 ])
              </form>
        </div> 
    </main>

@endsection
