@extends('layouts.app')

@section('content')

    <main class="pri-pad">  
        <div class="container"> 
            @include('property.create')
              <form method="post" class="property-submit" action="/amenities/{{$property->slug}}" >
                 @include('form_partials.amenities', [
                   'submitButton' => 'Create Amenity '
                 ])
              </form>
        </div> 
    </main>

@endsection
