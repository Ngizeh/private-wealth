@extends('layouts.app')

@section('content')

    <main class="pri-pad">  
        <div class="container"> 
            @include('property.create')
             @include('layouts.errors')
              <form method="post" class="property-submit" action="/agent" >
                 @include('form_partials.agent_details', [
                   'submitButton' => 'Create Agent '
                 ])
              </form>
        </div> 
    </main>

@endsection

