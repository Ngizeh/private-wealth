	<div class="property-wrap mb-20">
		@forelse( $properties as $property )
		<div class="ppt-list list-vw mb-30 featured">
			<figure>
				<span class="tag left text-uppercase bg-dark">Ksh. {{number_format($property->price)}}</span>
				<span class="tag right text-uppercase primary-bg">{{$property->status}}</span>
				<a href="/property/{{$property->slug}}" class="image-effect overlay">
					@foreach($property->medias as $set)
					@if($loop->first)
					<img src="/{{$set->thumbnail_path}}" alt="">
					@endif
					@endforeach
				</a>
			</figure>
			<!--fig-->

			<div class="content">
				<h4 class="mb-0"><a href="/property/{{$property->slug}}">{{$property->title}}</a></h4>
				<div class="mb-15">{{$property->location}}</div>
				<div style="justify-content: space-around">
					<div class="content-wrap" style="height: 8rem">
						<p>
							{{ $property->short_description }}
						</p>
					</div>
					<!--content-->
	
					<a href="/property/{{$property->slug}}" class="btn btn-sucess faa-parent animated-hover">
						View Details <i class="fa fa-long-arrow-right faa-passing"></i>
					</a>
				</div>
			</div>
			<!--content-->

			<div class="info">
				<ul>
					<li>Area &nbsp;&nbsp;-&nbsp;&nbsp; <span> {{$property->area}} sq/ft </span></li>
					<li>Bathrooms &nbsp;&nbsp;-&nbsp;&nbsp; <span>{{$property->bathroom}}</span> </li>
					<li>Bedrooms &nbsp;&nbsp;-&nbsp;&nbsp; <span>{{$property->bedroom}}</span> </li>
					<li>Floors &nbsp;&nbsp;-&nbsp;&nbsp; <span>{{$property->floor}}</span> </li>
				</ul>

				<a href="#" class="btn btn-link pull-right">
					<i class="fa fa-heart-o"></i> Add to Wishlist
				</a>
			</div>
		</div>
		<!--single property-->
		@empty
			<p>We don't have this preference yet</p>
		@endforelse
	</div>
	<!--property list-->

	 @include('includes.pagination')
	<!--pegination-->
</div>
<!--left block-->

 @include('includes.sidebar')
<!--sidebar-->