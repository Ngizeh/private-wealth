 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<!-- meta -->
 	<meta charset="utf-8">
 	<meta http-equiv="x-ua-compatible" content="ie=edge">
 	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
 	<title>Private Wealth Real Estate | Properties for Sale | Houses for Sale | Land for Sale</title>
 	<meta name="keywords" content="">
 	<meta name="description" content="">
 	<meta name="csrf-token" content="{{ csrf_token() }}">
 	<meta name="author" content="info@privatewealth.co.ke">
 	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/dropzone.css">
 	<link rel="shortcut icon" type="image/png" href="{{asset('assets/images/favicon.png')}}" />
 	<link rel="shortcut icon" type="image/png" href="{{asset('assets/images/favicon.png')}}" />
 	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700%7CRoboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

 	<link rel="stylesheet"  href="{{asset('/css/app.css')}}"/>
 	<link rel="stylesheet"  href="{{asset('assets/styles/font-awesome.min.css')}}">
 	<link rel="stylesheet"  href="{{asset('assets/styles/font-awesome-animation.min.css')}}">
 	<link rel="stylesheet"  href="{{asset('assets/styles/jquery-ui.min.css')}}" />
 	<link rel="stylesheet"  href="{{asset('assets/styles/pe-icon-7-stroke.css')}}" />
 	<link rel="stylesheet"  href="{{asset('assets/styles/slick.css')}}" />
 	<link rel="stylesheet"  href="{{asset('assets/styles/slick-theme.css')}}" />
 	<link rel="stylesheet"  href="{{asset('assets/styles/jquery.bxslider.css')}}" />
 	<link rel="stylesheet"  href="{{asset('assets/styles/ekko-lightbox.css')}}" />
 	<link rel="stylesheet"  href="{{asset('assets/styles/font.css')}}" />

 	<!-- Main css -->

 	<link rel="stylesheet"  href="{{asset('assets/styles/color-skin/default.css')}}" />
 	<link rel="stylesheet"  href="{{asset('assets/styles/responsive.css')}}" />
 	<script>
 		!function(f,b,e,v,n,t,s)
 		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
 			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
 			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
 			n.queue=[];t=b.createElement(e);t.async=!0;
 			t.src=v;s=b.getElementsByTagName(e)[0];
 			s.parentNode.insertBefore(t,s)}(window, document,'script',
 				'https://connect.facebook.net/en_US/fbevents.js');
 			fbq('init', '151863500371525');
 			fbq('track', 'PageView');
 		</script>
 		<script src="https://unpkg.com/turbolinks"></script>
 		<noscript>
 			<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=151863500371525&ev=PageView&noscript=1"/>
 		</noscript>

 	</head>
 	<body>
 		<div id="app">
 			@include('layouts.header')
 			@yield('content')
 			@include('layouts.footer')
 			@yield('scripts.footer')
 		</div>
 		<script src="{{asset('assets/js/jquery-3.1.1.min.js')}}"></script>
 		<script src="{{asset('assets/js/modernizr.js')}}"></script>
 		<script src="{{asset('assets/js/jquery-ui.js')}}"></script>
 		<script src="{{asset('assets/js/jquery-ui-touch-punch.js')}}"></script>
 		<script src="{{asset('assets/js/masonry.pkgd.min.js')}}"></script>
 		<script src="{{asset('assets/js/slick.min.js')}}"></script>
 		<script src="{{asset('assets/js/jquery.bxslider.min.js')}}"></script>
 		<script src="{{asset('assets/js/wow.min.js')}}"></script>
 		<script src="{{asset('assets/js/ekko-lightbox.min.js')}}"></script>
 		<script src="{{asset('assets/js/parallax.js')}}"></script>
 		<script src="{{asset('assets/js/main.js')}}"></script>
 		<script src="{{asset('js/app.js')}}"></script>
 	</body>
 	</html>

