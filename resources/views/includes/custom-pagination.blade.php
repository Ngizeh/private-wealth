 @if($paginator->hasPages())
	 <ul class="pagination">
	 	@if($paginator->onFirstPage())
		 	<li class="page-item page-link disabled" aria-label="true"
		 	aria-label="@lang('pagination.previous')">
		 		<span class="fa fa-caret-left" aria-hidden="true"> </span>
		 	</li>
		 	@else
		 	<li class="page-item">
		 		<a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="@lang('pagination.previous')">
		 			<span class="fa fa-caret-left"> </span>
		 		</a>
		 	<li>
	 	@endif

	 	@foreach ($elements as $element)
	 		@if(is_string($element))
	 			<li class="page-item page-link disabled" aria-disable="true">
	 				<span>{{ $element }}</span>
	 			</li>
	 		@endif
	 		@if(is_array($element))
	 			@foreach ($element as $page => $url)
	 				@if($page == $paginator->currentPage())
			 			<li class="page-item active" aria-current="page">
			 				<a class="page-link">{{ $page }} </a>
			 			</li>
	 				@else
			 			<li class="page-item">
			 				<a class="page-link" href="{{ $url }}">{{ $page }}</a>
			 			</li>
	 				@endif
	 			@endforeach
	 		@endif
	 	@endforeach
	 	@if($paginator->hasMorePages())
			<li class="page-item">
				<a class="page-link" href="{{ $paginator->nextPageUrl() }}" aria-label="@lang('pagination.next')" rel="next">
					<span class="fa fa-caret-right"> </span>
				</a>
			</li>
	 	@else
			<li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
					<a class="page-link fa fa-caret-right aria-hidden="true> </a>
				</a>
			</li>
	 	@endif
	 </ul>
 @endif

