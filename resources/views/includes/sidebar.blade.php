<div class="col-md-3 col-sm-3 sidebar">
	<div class="alert-message sidebar-form mb-20 ">
		<h6>Get daily property email alert in your inbox</h6>
		<form action="#">
			<div class="form-group">
				<input type="text" placeholder="Enter your email address">
			</div>
			<!--text-->

			<div class="form-group">
				<button type="submit">
					create alert <i class="fa fa-long-arrow-right"></i>
				</button>
			</div>
		</form>
	</div>
	<!--alert-->

	<div class="sidebar-filter sidebar-form">
		<h6 class="title">Find your home</h6>
		<form action="search" method="get">
			<div class="form-group select">
				<div class="form-group select">
					<select name="t">
						<option>Select Location </option>
						<option>Nairobi </option>
						<option>Kisumu</option>
						<option>Mombasa</option>
						<option>Kakamega</option>
					</select>
				</div>
				<!--location-->

				<select name="status">
					<option name="">Select Status </option>
					<option name="for sale">For sale</option>
					<option name="for rent">Rental</option>
				</select>
			</div>
			<!--status-->



			<div class="form-group select">
				<select name="p">
					<option selected disabled>Select Type</option>
					@foreach( App\Utilities\PropertyTypes::all() as $types )
					<option value="{{$types}}">
						{{$types}}
					</option>
					@endforeach
				</select>
			</div>
			<!--type-->
		
			<div class="form-group range">
				<select name="h">
					<option>Any Price</option>
					<option value="3000000">3,000,000</option>
					<option value="4000000">4,000,000</option>
					<option value="5000000">4,000,000</option>
				</select>
			</div>
			<!--price-->

			<div class="form-group">
				<button type="submit">
					Start Search <i class="fa fa-long-arrow-right"></i>
				</button>
			</div>
		</form>
	</div>
	<!--filter-->
</div>