<div class="box border agent-box  bg-dark">
    <h4 class="title mb-20 text-white">Agent Details</h4>
    <div class="wrap">
        <!-- <figure><img src="assets/images/box-agent.jpg" alt=""></figure> -->
        <div class="content-wrap mb-0 text-white">
            <h6 class="font-primary text-white">
                {{ucfirst($property->user->name)}}
                <span> Spring Valley Business Park, Nairobi, Kenya</span>
            </h6>

            <ul class="info">
                <li>E-mail : info@privatewealthre.co.ke</li>
                <li>Web : http://privatewealthre.co.ke</li>
                <li>Phone : +254-715-008-946</li>
                <li>Company : Private Wealth Real Eastate </li>
                <li>Listings : {{$property->count()}}</li>
            </ul>

{{--            <ul class="social-icons">--}}
{{--                <li>--}}
{{--                    <a href="#"><i class="fa fa-facebook"></i></a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#"><i class="fa fa-twitter"></i></a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#"><i class="fa fa-instagram "></i></a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#"><i class="fa fa-linkedin"></i></a>--}}
{{--                </li>--}}
{{--            </ul>--}}
        </div>
    </div>
</div>
