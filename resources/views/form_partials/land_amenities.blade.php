<div class="row mb-60">
    <div class="form-group col-sm-12">
        <div class="checkbox-wrap">
            <input name="fencing"  type="checkbox" value="1" {{ old('fencing', $land_amenity->fencing) == '1' ? 'checked' : 0 }}>
            <span>Fencing</span>
        </div>

        <div class="checkbox-wrap">
            <input name="water"  type="checkbox" value="1" {{ old('water', $land_amenity->water) == '1' ? 'checked' : 0 }}>
            <span>Water</span>
        </div>

        <div class="checkbox-wrap">
            <input name="electricity"  type="checkbox" value="1" {{ old('electricity', $land_amenity->electricity) == '1' ? 'checked' : 0 }}>
            <span>Electricity</span>
        </div>

        <div class="checkbox-wrap">
            <input name="access_road"  type="checkbox" value="1" {{ old('access_road', $land_amenity->access_road) == '1' ? 'checked' : 0 }}>
            <span>Access Road</span>
        </div>

        <div class="checkbox-wrap">
            <input name="existing_building"  type="checkbox" value="1" {{ old('existing_building' , $land_amenity->existing_building) == '1' ? 'checked' : 0 }}>
            <span>Existing Building</span>
        </div>
    </div>
</div>
<!-- Property Features-->
<div class="row">
    <div class="col-md-3">
        <button type="submit" class="btn btn-sucess btn-lg shadow faa-parent animated-hover">
            {{ $amenities }} <i class="fa fa-long-arrow-right faa-passing"></i>
        </button>
    </div>
</div>
