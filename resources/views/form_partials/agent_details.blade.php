@csrf
<h6 class="mb-15">Agent Details</h6>
<div class="row">
    <div class="form-group col-sm-4 mb-30">
        <label>Contact Person/Company Name</label>
        <input name="contact_person" placeholder="Contact Person Details" type="text">
    </div>
    <!--contact person-->

    <div class="form-group col-sm-4 mb-30">
        <label>Web Address</label>
        <input name="web_address"  placeholder="www.private.com" type="text">
    </div>
    <!--address-->

    <div class="form-group col-sm-4 mb-30">
        <label>Email</label>
        <input name="email" placeholder="johndoe@mail.com" type="text">
    </div>
    <!--email-->

    <div class="form-group col-sm-4 mb-30">
        <label>Phone Number</label>
        <input name="phone_number"  placeholder="+2547123654789" type="text">
    </div>

    <div class="form-group col-sm-4 mb-30">
        <label>Featured Image <span>*</span></label>
        <input type="file" name="file-7[]" id="file-7" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
        <label for="file-7"><span class="text"></span> <span class="btn btn-lg">Browse</span></label>
    </div>
    <!--Phone-->
</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            <a href="" class="btn btn-warning btn-sm shadow faa-parent animated-hover">
                <i class="fa fa-long-arrow-left faa-passing"></i> Back
            </a>
        </div>
        <div class="col-md-3">
            <button type="submit" href="/property/feature" class="btn btn-sucess btn-sm shadow faa-parent animated-hover">
                {{ $submitButton ?? 'Next' }}
                <i class="fa fa-long-arrow-right faa-passing"></i>
            </button>
        </div>
    </div>
</div>

<!--agent detail-->
