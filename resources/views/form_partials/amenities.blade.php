@csrf
<h6 class="mb-15"> Amenities</h6>
<div class="row mb-60">
    <div class="form-group col-sm-12">
        <div class="checkbox-wrap">
            <input name="gym" type="checkbox" value="1" {{ old('gym', $amenity->gym) == '1' ? 'checked' : 0 }}>
            <span>Gym</span>
        </div>

        <div class="checkbox-wrap">
            <input name="swimming" type="checkbox" value="1" {{ old('swimming', $amenity->swimming) == '1' ? 'checked' : 0 }}>
            <span>Swimming</span>
        </div>

        <div class="checkbox-wrap">
            <input name="cctv" type="checkbox" value="1" {{ old('cctv', $amenity->cctv) == '1' ? 'checked' : 0 }}>
            <span>CCTV</span>
        </div>

        <div class="checkbox-wrap">
            <input name="scenic_view" type="checkbox" value="1" {{ old('scenic_view', $amenity->scenic_view) == '1' ? 'checked' : 0 }}>
            <span>Scenic View</span>
        </div>

        <div class="checkbox-wrap">
            <input name="soler_panel" type="checkbox" value="1" {{ old('soler_panel', $amenity->soler_panel) == '1' ? 'checked' : 0 }}>
            <span>Soler Panel</span>
        </div>
        <div class="checkbox-wrap">
            <input name="bio_digestor" type="checkbox" value="1" {{ old('bio_digestor', $amenity->bio_digestor) == '1' ? 'checked' : 0 }}>
            <span>Bio Digestor</span>
        </div>

        <div class="checkbox-wrap">
            <input name="underground" type="checkbox" value="1" {{ old('underground', $amenity->underground) == '1' ? 'checked' : 0 }}>
            <span>Underground</span>
        </div>

        <div class="checkbox-wrap">
            <input name="internet" type="checkbox" value="1" {{ old('internet', $amenity->internet) == '1' ? 'checked' : 0 }}>
            <span>Internet</span>
        </div>

        <div class="checkbox-wrap">
            <input name="lift_elevator" type="checkbox" value="1" {{ old('lift_elevator', $amenity->lift_elevator) == '1' ? 'checked' : 0 }}>
            <span>Lift Elevator</span>
        </div>

        <div class="checkbox-wrap">
            <input name="pets_allowed" type="checkbox" value="1" {{ old('pets_allowed', $amenity->pets_allowed) == '1' ? 'checked' : 0 }}>
            <span>Pets Allowed</span>
        </div>
        <div class="checkbox-wrap">
            <input name="balcony" type="checkbox" value="1" {{ old('balcony', $amenity->balcony) == '1' ? 'checked' : 0 }}>
            <span>Balcony</span>
        </div>

        <div class="checkbox-wrap">
            <input name="alarm_backup" type="checkbox" value="1" {{ old('alarm_backup', $amenity->alarm_backup) == '1' ? 'checked' : 0 }}>
            <span>Alarm Backup</span>
        </div>

        <div class="checkbox-wrap">
            <input name="wifi" type="checkbox" value="1" {{ old('wifi', $amenity->wifi) == '1' ? 'checked' : 0 }}>
            <span>Wifi</span>
        </div>

        <div class="checkbox-wrap">
            <input name="bore_hole" type="checkbox" value="1" {{ old('bore_hole', $amenity->bore_hole) == '1' ? 'checked' : 0 }}>
            <span>Bore Hole</span>
        </div>

        <div class="checkbox-wrap">
            <input name="electric_fence" type="checkbox" value="1" {{ old('electric_fence', $amenity->electric_fence) == '1' ? 'checked' : 0 }}>
            <span>Electric Fence</span>
        </div>

        <div class="checkbox-wrap">
            <input name="garden" type="checkbox" value="1" {{ old('garage', $amenity->garden) == '1' ? 'checked' : 0 }}>
            <span>Garden</span>
        </div>

        <div class="checkbox-wrap">
            <input name="build_in_cupboard" type="checkbox" value="1" {{ old('build_in_cupboard', $amenity->build_in_cupboard) == '1' ? 'checked' : 0 }}>
            <span>Build in Cupboard</span>
        </div>

        <div class="checkbox-wrap">
            <input name="ensuite" type="checkbox" value="1" {{ old('ensuite', $amenity->ensuite) == '1' ? 'checked' : 0 }}>
            <span>Ensuite</span>
        </div>

        <div class="checkbox-wrap">
            <input name="water_tank" type="checkbox" value="1" {{ old('water_tank', $amenity->water_tank) == '1' ? 'checked' : 0 }}>
            <span>Water Tank</span>
        </div>

        <div class="checkbox-wrap">
            <input name="walkin_closet" type="checkbox" value="1" {{ old('walkin_closet', $amenity->walkin_closet) == '1' ? 'checked' : 0 }}>
            <span>Walkin Closet</span>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-md-6">
                <a href="/property/create" class="btn btn-warning btn-sm shadow faa-parent animated-hover">
                    <i class="fa fa-long-arrow-left faa-passing"></i> Back
                </a>
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-sucess btn-sm shadow faa-parent animated-hover">
                    {{ $submitButton ?? 'Next' }} <i class="fa fa-long-arrow-right faa-passing"></i>
                </button>
            </div>
        </div>
    </div>
