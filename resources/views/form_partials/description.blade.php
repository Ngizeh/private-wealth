@csrf
<h6 class="mb-15">Property Description and Price</h6>
<div class="row mb-55">
    <div class="form-group col-sm-4 mb-30">
        <label>Property Title <span>*</span></label>
        <input type="text" value="{{old('title', $property->title)}}"name="title">
    </div>
    <!--title-->

    <div class="form-group col-sm-4 mb-30">
        <label>Property location <span>*</span></label>
        <input type="text" value="{{old('location', $property->location)}}"name="location">
    </div>

    <div class="form-group col-sm-4 mb-30">
        <label>Property Price  <span>*</span></label>
        <input type="text" value="{{old('price', $property->price)}}"name="price">
    </div>

    <!--- location --->

    <div class="form-group select col-sm-4 mb-30">
        <label>Property Type <span>*</span></label>
        <select name="type">
            <option selected disabled>Choose a Property Type</option>
            @foreach( App\Utilities\PropertyTypes::all() as $types )
                <option value="{{$types}}" {{old('type', $property->type) == $types ? 'selected' : ''}}>
                    {{$types}}
                </option>
            @endforeach
        </select>
    </div>
    <!--type-->

    <div class="form-group select col-sm-4 mb-30">
        <label>Property Status <span>*</span></label>
        <select name="status">
            <option selected disabled>Choose Status</option>
            @foreach( App\Utilities\Status::all() as $stat )
                <option value="{{$stat}}" {{old('status', $property->status) == $stat ? 'selected' : ''}}>
                    {{ ucfirst($stat) }}
                </option>
            @endforeach
        </select>
    </div>

    <!--status-->

    <div class="form-group col-sm-4 mb-30">
        <label>Listed For </label>
        <select name="listed_for">
            <option selected disabled>Choose the Listings </option>
            @foreach( App\Utilities\ListedFor::all() as $listed )
                <option value="{{$listed}}" {{old('listed_for', $property->listed_for) == $listed ? 'selected' : ''}}>
                    {{$listed}}
                </option>
            @endforeach
        </select>
    </div>

    <!-- Listed for  -->

    <div class="form-group col-sm-12 mb-30">
        <label>Property Descriptions <span>*</span></label>
        <textarea name="description">{{old('description', $property->description)}}</textarea>
    </div>

    <!-- Description  -->
</div>

<h6 class="mb-15">Extra Information</h6>
<div class="row mb-55">
    <div class="form-group col-sm-4 mb-30">
        <label>Area (Sq/ft)</label>
        <input type="text" value="{{old('area', $property->area)}}" name="area">
    </div>
    <div class="form-group col-sm-4 mb-30">
        <label>Bedrooms</label>
        <input type="text" value="{{old('bedroom', $property->bedroom)}}" name="bedroom">
    </div>
    <!--bed rooms-->

    <div class="form-group col-sm-4 mb-30">
        <label>Bathrooms</label>
        <input type="text" value="{{old('bathroom', $property->bathroom)}}" name="bathroom">
    </div>
    <!--bathroom-->

    <div class="form-group col-sm-4 mb-30">
        <label>Parking</label>
        <input type="text" value="{{old('parking', $property->parking)}}" name="parking">
    </div>
    <!--Parking-->
</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-3">
            <a href="/user-dashboard">
            <i class="fa fa-long-arrow-left faa-passing"></i>
            Back
            </a>
        </div>
        <div class="col-md-3 md-offset-2">
            <button  type="submit" href="/property/features" class="btn btn-sucess btn-sm shadow faa-parent animated-hover">
                {{ $submitButton ?? 'Next' }} <i class="fa fa-long-arrow-right faa-passing"></i>
            </button>
        </div>
    </div>

</div>

