@csrf
<h6 class="mb-15">Property Location</h6>
<div class="row mb-55">
    <div class="form-group col-sm-4 mb-30">
        <label>Address</label>
        <input type="text" name="address">
    </div>
    <!--Address-->

    <div class="form-group col-sm-4 mb-30">
        <label>State/Town</label>
        <input type="text" name="state">
    </div>

    <!--State-->

    <div class="form-group col-sm-4 mb-30">
        <label>County</label>
        <select name="county">
            <option selected disabled>Select the County </option>
            @foreach( App\Utilities\Counties::all() as $county )
                <option value="{{$county}}"> {{$county}}</option>
            @endforeach
        </select>
    </div>
    <!--Country-->

    <div class="form-group col-sm-4 mb-30">
        <label>Postal Code / Zip</label>
        <input type="text" name="zip_code">
    </div>
    <!--Postal Code / Zip-->

    <div class="form-group col-sm-4 mb-30">
        <label>Latitude</label>
        <input type="text" id="lat"name="lat" readonly="readonly" value="{{old('lat')}}">
    </div>
    <!--latitude-->

    <div class="form-group col-sm-4 mb-30">
        <label>Longitude</label>
        <input type="text" id="lng"name="lng" readonly="readonly" value="{{old('lng')}}">
    </div>
    <!--longitude-->

    <div class="col-sm-12 mb-30">
        <div class="map-hold">
            <div id="map-canvas" style="height:450px; width: 100%" class="form-control"></div>
            <!--     <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d96708.3419418377!2d-74.03927127918426!3d40.75904032921492!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2588f046ee661%3A0xa0b3281fcecc08c!2sManhattan%2C+New+York%2C+NY%2C+USA!5e0!3m2!1sen!2snp!4v1497343556535" width="600" height="450"></iframe> -->
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-6">
            <a href="/" class="btn btn-warning btn-sm shadow faa-parent animated-hover">
                <i class="fa fa-long-arrow-left faa-passing"></i> Back
            </a>
        </div>
        <div class="col-md-3">
            <button type="submit" href="/property/images" class="btn btn-sucess btn-sm shadow faa-parent animated-hover">
                {{ $submitButton ?? 'Next' }} <i class="fa fa-long-arrow-right faa-passing"></i>
            </button>
        </div>
    </div>
</div>


<script>
    var Nairobi = {lat: -1.2920659, lng: 36.82194619999996};
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 15,
        center: Nairobi
    });
    var marker = new google.maps.Marker({
        position: Nairobi,
        map: map,
        draggable: true
    });
    var searchBox = new google.maps.places.Autocomplete(document.getElementById('searchmap'));
    google.maps.event.addListener(searchBox,'places_changed',function(){
        var places = searchBox.getPlaces();
        var bounds = new google.maps.LatLngBounds();
        var i, place;
        for (i=0; place=places[i];i++) {
            bounds.extend(place.geometry.location);
            marker.setPosition(place.geometry.location);
        }
        map.fitBounds(bounds);
        map.setZoom(15);
    });
    google.maps.event.addListener(marker, 'position_changed', function () {
        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();
        $('#lat').val(lat);
        $('#lng').val(lng);
    });
</script>


