
@csrf
<h6 class="mb-15">Property Description</h6>
<div class="row mb-55">
    <div class="form-group col-sm-4 mb-30">
        <label>Location <span>*</span></label>
        <input type="text" name="location"  value="{{old('location', $land->location)}}" required >
    </div>
    <!--title-->

    <div class="form-group select col-sm-4 mb-30">
        <label>Size of the Land <span>*</span></label>
        <input type="text" name="size"  value="{{old('size', $land->size)}}" required >
    </div>
    <!--type-->

    <div class="form-group select col-sm-4 mb-30">
        <label>Price <span>*</span></label>
        <input type="text" name="price"  value="{{old('price', $land->price)}}" required >
    </div>
    <!--status-->

    <div class="form-group col-sm-4 mb-30">
        <label>Title Available <span>*</span></label>
        <select name="title">
            <option selected disabled>Choose Availability</option>
            @foreach( App\Utilities\AvailableTitle::all() as $tit )
                <option value="{{ $tit }}" {{old('title', $land->title) == $tit ? 'selected' : ''}} >
                    {{ $tit }}
                </option>
            @endforeach
        </select>
    </div>
    <!--location-->

    <div class="form-group col-sm-12 mb-30">
        <label>Property Descriptions <span>*</span></label>
        <textarea name="description" required>{{old('description', $land->description)}}</textarea>
    </div>
</div>
<!--property description-->

<div class="row">
    <div class="col-md-2">
        <button type="submit" class="btn btn-sucess btn-lg shadow faa-parent animated-hover">
            {{ $submit }} <i class="fa fa-long-arrow-right faa-passing"></i>
        </button>
    </div>
</div>

