@extends('layouts.app')

@section('content')

    <main class="pri-pad">
        <div class="container">
            <h3 class="mb-30">Create Land Property</h3>

            @include('includes.errors')

            <form action="/lands-property" method="post" class="property-submit">
                @csrf
               @include('form_partials.land', ['submit' => 'Submit Land'])
            </form>
        </div>
    </main>

@endsection
