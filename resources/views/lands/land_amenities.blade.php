@extends('layouts.app')

@section('content')
    <div class="container" style="padding:20px 20px">
        <h6 class="mb-15">Land Amenities</h6>
        <form action="/lands-amenity/{{$land->slug}}" method="post" class="property-submit">
            @csrf
           @include('form_partials.land_amenities',  ['amenities' => 'Add Amenities'])
        </form>
    </div>

@endsection
