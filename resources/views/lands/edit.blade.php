@extends('layouts.app')

@section('content')

    <main class="pri-pad">
        <div class="container">
            <h3 class="mb-30">Edit Land Property</h3>

            @include('includes.errors')

            <form action="/lands-property/{{$land->slug}}" method="post" class="property-submit">
                @csrf
                @method('patch')
                @include('form_partials.land', ['submit' => 'Edit Land'])
            </form>
        </div>
    </main>

@endsection
