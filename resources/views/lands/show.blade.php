@extends('layouts.app')

@section('content')

    <main>
        <div class="filter-bar filter-bar-2 primary-bg text-white">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <form action="#" class="form-inline">
                            <div class="form-group">
                                <input type="text" placeholder="Enter Keywords">
                            </div>
                            <!--keyword-->

                            <div class="form-group select">
                                <label>Location</label>
                                <select>
                                    <option>Nairobi</option>
                                    <option>Mombasa</option>
                                    <option>Kisumu</option>
                                    <option>Kakamega</option>
                                </select>
                            </div>
                            <!--location-->

                            <div class="form-group select">
                                <label>property type</label>
                                <select>
                                    <option>Apartment</option>
                                    <option>House</option>
                                </select>
                            </div>
                            <!--type-->

                            <div class="form-group select">
                                <label>Min Price</label>
                                <select>
                                    <option>any Price</option>
                                </select>
                            </div>
                            <!-- min price-->

                            <div class="form-group select">
                                <label>Max Price</label>
                                <select>
                                    <option>any Price</option>
                                </select>
                            </div>
                            <!--max price-->

                            <div class="form-group">
                                <button type="submit">
                                    Start Search <i class="fa fa-long-arrow-right"></i>
                                </button>
                            </div>
                            <!--max price-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--filter bar-->

        <div class="gallery-wrap full">
            <div class="banner-content">
                <div class="container">
                    <div class="title text-white">
                        <h2 class="mb-0 lh-normal text-white">{{$property->location}}</h2> Area : {{$property->size}} acre
                        <span class="price primary-bg text-white">Ksh. {{number_format($property->price)}}</span>
                    </div>
                </div>
            </div>

            <ul class="gallery-full">
                @foreach($property->landMedias as $set)
                    <li>
                        <figure class="text-center overlay overlay2 resize-image">
                            <img src="/{{$set->path}}" alt="">
                        </figure>
                    </li>
                @endforeach
            </ul>
        </div>
        <!--gallery wrap-->

        <section class="pri-pad">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-9 property-single left-block ">
                        <div class="row mb-40">
                            <div class="col-sm-4 left-block ppt-info">
                                <div class="box border">
                                    <div class="box-title">
                                        <h6 class="mb-0 lh-normal">Property Informations</h6></div>
                                    <div class="detail info">
                                        <ul>
                                            <li>Land Identity<span>{{$property->property_identity}} </span></li>
                                            <li>Location<span>{{$property->location}} </span></li>
                                            <li>Price <span>{{number_format($property->price)}}</span> </li>
                                            <li>Size <span>{{$property->size}}</span> </li>
                                            <li>Area <span>{{$property->area}}sq/ft </span></li>
                                            <li>Title<span>{{$property->title == 1 ? 'Yes': 'No'}} </span> </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--property-desc-->

                            <div class="col-sm-8 ppt-desc">
                                <div class="box border content-wrap mb-0">
                                    <h4 class="title mb-20">Property Descriptions</h4>
                                    <p>
                                        {{$property->description}}
                                    </p>

                                </div>
                            </div>
                            <!--property-desc-->
                        </div>
                        <!--property detail-->

                        <div class="box border mb-40">
                            <h4 class="title mb-20">Amenities</h4>
                            <ul class="amenities">
                                @foreach($property->landAmenities as  $amenity)
                                    <li>{!! $amenity->water ? 'Water' : '<del>Water</del>' !!} </li>
                                    <li>{!! $amenity->electricity ? 'Electricity' : '<del>Electricity</del>' !!} </li>
                                    <li>{!! $amenity->fencing ? 'Fencing' : '<del>Fencing</del>' !!} </li>
                                    <li>{!! $amenity->access_road ? 'Access Road' : '<del>Access Road</del>' !!} </li>
                                    <li>{!! $amenity->exiting_building ? ' Exisitng Building' : '<del> Exisitng Building</del>' !!} </li>
                                @endforeach
                            </ul>
                        </div>
                        <!--aminities-->

                        <!--    <div class="box border mb-40">
                               <h4 class="title mb-20">Location Map</h4>
                                  <div class="map-hold">
                                    <div id="map-canvas" style="height:450px; width: 100%" class="form-control"></div>
                              </div>
                           </div> -->
                        <!--map hold-->

                    @include('includes.agent_details')
                    <!--agent detail-->
                    </div>
                    <!--left block-->

                    <div class="col-md-3 col-sm-3 sidebar">
                        <div class="alert-message sidebar-form mb-20 ">
                            <h6>Get daily property email alert in your inbox</h6>
                            <form action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Enter your email address">
                                </div>
                                <!--text-->

                                <div class="form-group">
                                    <button type="submit">
                                        create alert <i class="fa fa-long-arrow-right"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!--alert-->

                        <div class="sidebar-filter sidebar-form">
                            <h6 class="title">Find your home</h6>
                            <form action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Enter search keyword ">
                                </div>
                                <!--keyword-->

                                <div class="form-group select">
                                    <select>
                                        <option>Select Status </option>
                                    </select>
                                </div>
                                <!--status-->

                                <div class="form-group select">
                                    <select>
                                        <option>Select Location</option>
                                    </select>
                                </div>
                                <!--location-->

                                <div class="form-group select">
                                    <select>
                                        <option>Select Types </option>
                                    </select>
                                </div>
                                <!--type-->

                                <div class="form-group select">
                                    <select>
                                        <option>No. of Bedrooms</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                                <!--bed room-->

                                <div class="form-group select">
                                    <select>
                                        <option>No. of Bathrooms</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                                <!--bathroom-->

                                <div class="form-group select">
                                    <select>
                                        <option>No. of Floors</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                    </select>
                                </div>
                                <!--floors-->

                                <div class="form-group range">
                                    <label>Price range</label>
                                    <div id="slider-range"></div>
                                    <input type="hidden" id="amount1">
                                    <input type="hidden" id="amount2">
                                    <div id="amount"></div>
                                </div>
                                <!--price-->

                                <div class="form-group range">
                                    <label>Area</label>
                                    <div id="area-range"></div>
                                    <input type="hidden" id="area1">
                                    <input type="hidden" id="area2">
                                    <div id="area"></div>
                                </div>
                                <!--area-->

                                <div class="form-group">
                                    <button type="submit">
                                        Start Search <i class="fa fa-long-arrow-right"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!--filter-->
                    </div>
                    <!--sidebar-->
                </div>
            </div>
        </section>
        <!--property wrap-->
    </main>

@endsection

