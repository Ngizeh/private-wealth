@extends('layouts.app')

@section('content')
<div class="container" style="padding:20px 20px">
    <h6 class="mb-15">Edit Land Amenities</h6>
    <form action="/lands-amenities/{{$land->slug}}" method="post" class="property-submit">
        @csrf

        @method('patch')

        @include('form_partials.land_amenities', ['amenities' => 'Edit Amenities'])
    </form>
</div>

@endsection
