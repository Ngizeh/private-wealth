@extends('layouts.app')

@section('content')

<main>
	<section class="pri-pad">
		<div class="container">
			<div class="row">
				<div class="col-md-9 col-sm-9 left-block">
					<div class="sec-title icon-wrap">
						<h3>Our Lands</h3>
					</div>
					<!--title-->

					<div class="property-wrap mb-20">
						@forelse( $properties as $property )
						<div class="ppt-list list-vw mb-30 featured">
							<figure>
								<span class="tag left text-uppercase bg-dark">Ksh. {{number_format($property->price)}}</span>
								<!--  <span class="tag right text-uppercase primary-bg">{{$property->status}}</span> -->
								<a href="/lands-property/{{$property->slug}}" class="image-effect overlay">
									@foreach($property->landMedias as $set)
									@if($loop->first)
									<img src="/{{$set->thumbnail_path}}" alt="{{ $property->location }}">
									@endif
									@endforeach
								</a>
							</figure>
							<!--fig-->

							<div class="content">
								<h4 class="mb-0"><a href="/lands-property/{{$property->slug}}">{{$property->location}}</a></h4>
								<div class="mb-15">Title Available :{{$property->title == 1 ? ' Yes ' : ' No '}}</div>

                                <div style="justify-content: space-around">
                                    <div class="content-wrap" style="height: 8rem">
                                        <p>
                                            {{ $property->short_description }}
                                        </p>
                                    </div>
                                    <!--content-->

                                    <a href="/lands-property/{{$property->slug}}"
                                       class="btn btn-sucess faa-parent animated-hover">
                                        View Details <i class="fa fa-long-arrow-right faa-passing"></i>
                                    </a></div>
							</div>
							<!--content-->

							<div class="info">
								<ul>
									<li>Size &nbsp;&nbsp;-&nbsp;&nbsp; <span> {{$property->size}} sq/ft </span></li>
									<li>Location &nbsp;&nbsp;-&nbsp;&nbsp; <span>{{$property->location}}</span> </li>
									<li>Title &nbsp;&nbsp;-&nbsp;&nbsp; <span>{{$property->title == 1 ? 'Yes' : 'No'}}</span> </li>
								</ul>

								<a href="#" class="btn btn-link pull-right">
									<i class="fa fa-heart-o"></i> Add to Wishlist
								</a>
							</div>
						</div>
						<!--single property-->
						@empty
						<p>We don't have this preference yet</p>
						@endforelse
					</div>
					<!--property list-->

					@include('includes.pagination')
					<!--pegination-->
				</div>
				<!--left block-->

				@include('includes.sidebar')
				<!--sidebar-->
			</div>
		</div>
	</section>
	<!--property wrap-->
</main>

@endsection
