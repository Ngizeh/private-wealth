@extends('layouts.app')

@section('content')

    <div class="container" style="padding: 4rem 4rem">
        <h3>Land Preview</h3>
        <div class="row">
            <p class="leading">
                Title : {{$land->location}}
            </<p>
            <hr>
            @foreach($land->landMedias->chunk(3) as $set)
                @foreach($set as $photo)
                    <div class="col-md-4 col-lg-4" style="padding-bottom: 20px">
                        <img src="/{{$photo->thumbnail_path}}" alt="">
                    </div>
                @endforeach
            @endforeach
            <div class="col-md-12">
                <div class="box border mb-40">
                    <h4 class="title mb-20">Descriptions</h4>
                    <p>
                        {{$land->description}}
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box border mb-40">
                    <h4 class="title mb-20">Land Information</h4>
                    <div class="detail info">
                        <ul>
                            <li>Land Identity<span>{{$land->property_identity}} </span></li>
                            <li>Location<span>{{$land->location}} </span></li>
                            <li>Price <span>{{$land->price}}</span> </li>
                            <li>Size <span>{{$land->size}}</span> </li>
                            <li>Area <span>{{$land->area}}sq/ft </span></li>
                            <li>Title<span>{{$land->title == 1 ? 'Yes': 'No'}} </span> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box border mb-40">
                    <h4 class="title mb-20">Amenities</h4>
                    <ul class="amenities">
                        @foreach($land->landAmenities as $key => $amenity)
                            <li>{!! $amenity->water ? 'Water' : '<del>Water</del>' !!} </li>
                            <li>{!! $amenity->electricity ? 'Electricity' : '<del>Electricity</del>' !!} </li>
                            <li>{!! $amenity->fencing ? 'Fencing' : '<del>Fencing</del>' !!} </li>
                            <li>{!! $amenity->access_road ? 'Access Road' : '<del>Access Road</del>' !!} </li>
                            <li>{!! $amenity->exiting_building ? ' Exisitng Building' : '<del> Exisitng Building</del>' !!} </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <a href="/lands-property/{{$land->slug}}/edit" class="btn btn-info">Edit</a>
            </div>
        </div>
        <div class="col-md-4">
            <form method="post" class="form-group" action="/land-preview/{{$land->slug}}" >
                {{csrf_field()}}

                {{method_field('patch')}}
                <input type="hidden" name="published_at" value="{{ now() }}">

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Publish the land</button>
                </div>
            </form>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    </div>

@endsection

