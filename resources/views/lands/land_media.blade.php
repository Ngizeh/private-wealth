@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($land->landmedias->chunk(3) as $set)
            <div class="row">
                @foreach($set as $photo)
                    <div class="col-md-4 col-lg-4">
                        <form method="post" action="/land-medias/{{$photo->id}}">
                            @csrf
                            {{ method_field('DELETE')}}
                            <div class="col-md-4">
                                <button type="submit" class="float-right btn btn-danger">Delete</button>
                            </div>
                        </form>
                        <img src="/{{$photo->thumbnail_path}}" alt="">
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>

    <div class="container">
        <div class="row">
            <h2 class="leading"> Upload Photos</h2>
            <form id="myDropzone" action="/land-medias/{{$land->slug}}"
                  class="dropzone" method="post" enctype="multipart/form-data">
                @csrf
            </form>
        </div>
        <div class="row" style="padding: 20px">
            <div class="col-md-3">
                <a href="{{ URL::previous() }}">Back</a>
            </div>
            <div class="col-md-3 offset-md-2">
                <a href="/land-preview/{{$land->slug}}" class="btn btn-primary">
                    Preview the Land
                </a>
            </div>
        </div>
    </div>
@stop
@section('scripts.footer')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
    <script>
        Dropzone.options.myDropZone = {
            paramName: 'file',
            maxFilesize: 100.0,
            acceptedFiles: '.jpg,.jpeg,.png, .bmp'
        };
    </script>
@stop

