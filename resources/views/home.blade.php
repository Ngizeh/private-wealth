@extends('layouts.app')

@section('content')
   <intro-section></intro-section>
   <main>
        <section class="intro pri-pad-b">
            <div class="container">
                <div class="filter-bar pri-pad-b">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form action="search" method="get" class="form-inline">
                                <div class="form-group select">
                                    <label>Location</label>
                                    <select name="t">
                                        <option>Nairobi</option>
                                        <option>Mombasa</option>
                                        <option>Kisumu</option>
                                    </select>
                                </div>
                                <!--location-->

                                <div class="form-group select">
                                    <label>property type</label>
                                    <select name="p">
                                        <option>Apartment</option>
                                        <option>Villa</option>
                                        <option>Condo</option>
                                    </select>
                                </div>
                                <!--type-->

                                <div class="form-group select">
                                    <label>property status</label>
                                    <select name="s">
                                        <option value="for sale">For Sale</option>
                                        <option value="for rent">For Rent</option>
                                    </select>
                                </div>
                                <!--type-->

                                <div class="form-group select">
                                    <label>Min Price</label>
                                    <select name="h">
                                        <option>any Price</option>
                                        <option value="2000000">2,000,000</option>
                                        <option value="3000000">3,000,000</option>
                                    </select>
                                </div>
                                <!-- min price-->

                                <div class="form-group select">
                                    <label>Max Price</label>
                                    <select name="h">
                                        <option>any Price</option>
                                        <option value="3000000">3,000,000</option>
                                        <option value="4000000">4,000,000</option>
                                        <option value="5000000">4,000,000</option>
                                    </select>
                                </div>
                                <!--max price-->

                                <div class="form-group">
                                    <button type="submit">
                                        Start Search <i class="fa fa-long-arrow-right"></i>
                                    </button>
                                </div>
                                <!--max price-->
                            </form>
                        </div>
                    </div>
                </div>
                <!--filter bar-->

                <div class="row intro-content">
                    <div class="col-md-7 col-sm-7">
                        <div class="content-wrap">
                            <h2>Fluent in Property Management</h2>
                            <p>
                              We offer various services leveraging on our experience, expertise and track record in the local Real Estate Market. We serve both institutional and Individual Investors seeking management and execution capacity to fulfill their Real Estate investment objectives.
                            </p>
                            <p>
                               We manage property for Corporate and Individual Investors in excess of Ksh. 1 billion.
                            </p>
                        </div>
                        <!--content-->
                        <a href="/about" class="btn btn-border btn-lg faa-parent animated-hover">
                            About Us
                        </a>
                        <a href="/property" class="btn btn-border btn-lg faa-parent animated-hover">
                           View Our Properties <i class="fa fa-long-arrow-right faa-passing"></i>
                        </a>
                    </div>
                </div>
                <!--intro content-->
            </div>
        </section>
        <!--intro-->

        <section class="featured-block featured-block-2 pri-pad">
            <!-- section container -->
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="main-title text-center">
                            <h2>Recently listed properties</h2>
                            <p></p>
                        </div>
                    </div>
                </div>
                <!--title-->

                <div class="row">
                    @foreach($properties as $property)
                    <div class="col-md-4 col-sm-4 card-height">
                        <div class="featured-wrap wrap mb-30">
                            <a href="/property/{{$property->slug}}" class="tag left text-uppercase primary-bg">{{$property->status}}</a>
                            <figure>
                                <a href="/property/{{$property->slug}}" class="image-effect overlay">
                                  @foreach($property->medias as $set)
                                  @if($loop->first)
                                  <img src="/{{$set->thumbnail_path}}" alt="{{ $property->title}}" >
                                  @endif
                                  @endforeach
                              </a>
                            </figure>
                            <!--fig-->

                            <div class="content-wrap mb-0">
                                <div class="title-wrap mb-10">
                                    <h4 class="mb-0"><a href="#">{{ $property->short_title }}</a></h4>
                                    <span>{{$property->location}}</span>
                                </div>

                                <div class="price-tag price">
                                    @if($property->price < 1000000)
                                    <small> Ksh. {{number_format($property->price)}}</small>
                                    @else
                                    <small> Ksh. {{number_format(($property->price)/1000000, 1)}} M</small>
                                    @endif
                                </div>

                                <p style="margin-top: 2rem;">
                                    {{$property->bedroom}} Bedroom - {{ $property->excerpt }}
                                </p>
                            </div>
                            <!--content-->

                            <a href="/property/{{$property->slug}}" class="btn btn-link faa-parent animated-hover">
                                View Details <i class="fa fa-long-arrow-right faa-passing"></i>
                            </a>
                        </div>
                    </div>
                    @endforeach
               </div>
                    <!--single featured-->

                <div class="btn-wrap text-center">
                    <a href="/property" class="btn btn-sucess btn-lg shadow faa-parent animated-hover">
                        More Properties <i class="fa fa-long-arrow-right faa-passing"></i>
                    </a>
                </div>
            </div>
        </section>
        <!--featured property-->

        <section class="near-by-property wrap2 bg-white pri-pad">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="main-title text-center">
                            <h2>Explore nearby neighborhood</h2>
                            <p>
                                We have a range of properties in several locations the major locations are Nairobi, Kisumu, Mombasa, Kakamega
                                Nakuru, Naivasha etc.
                            </p>
                        </div>
                    </div>
                </div>
                <!--title-->

                <div class="row">
                    <div class="col-md-8 col-sm-8 mb-30">
                        <div class="near-by-wrap wrap2">
                            <a href="#" class="link-overlay"></a>
                            <div class="content-wrap mb-0 text-white">
                                <div class="inner">
                                    <div class="title-wrap">
                                        <h4 class="mb-0 text-white">Nairobi</h4> 0 properties listed
                                    </div>
                                </div>
                            </div>

                            <figure class="image-effect overlay">
                                <img src="assets/images/nairobi-city.jpg" alt="">
                            </figure>
                            <!--fig-->
                        </div>
                    </div>
                    <!--single property-->

                    <div class="col-md-4 col-sm-4 mb-30 big pull-right">
                        <div class="near-by-wrap wrap2">
                            <a href="#" class="link-overlay"></a>
                            <div class="content-wrap mb-0 text-white">
                                <div class="inner">
                                    <div class="title-wrap">
                                        <h4 class="mb-0 text-white">Mombasa</h4> 0 properties listed
                                    </div>
                                </div>
                            </div>

                            <figure class="image-effect overlay">
                                <img src="assets/images/mombasa-city.jpg" alt="">
                            </figure>
                            <!--fig-->
                        </div>
                    </div>
                    <!--single property-->

                    <div class="col-md-4 col-sm-4 mb-30">
                        <div class="near-by-wrap wrap2">
                            <a href="#" class="link-overlay"></a>
                            <div class="content-wrap mb-0 text-white">
                                <div class="inner">
                                    <div class="title-wrap">
                                        <h4 class="mb-0 text-white">Kisumu</h4> 0 properties listed
                                    </div>
                                </div>
                            </div>

                            <figure class="image-effect overlay">
                                <img src="assets/images/kisumu-city.jpg" alt="">
                            </figure>
                            <!--fig-->
                        </div>
                    </div>
                    <!--single property-->

                    <div class="col-md-4 col-sm-4 mb-30">
                        <div class="near-by-wrap wrap2">
                            <a href="#" class="link-overlay"></a>
                            <div class="content-wrap mb-0 text-white">
                                <div class="inner">
                                    <div class="title-wrap">
                                        <h4 class="mb-0 text-white">Kakamega</h4> 0 properties listed
                                    </div>
                                </div>
                            </div>

                            <figure class="image-effect overlay">
                                <img src="assets/images/kakamega-city.png" alt="">
                            </figure>
                            <!--fig-->
                        </div>
                    </div>
                    <!--single property-->
                </div>
            </div>
        </section>
        <!--near by -->

        <section class="subscription">
            <div class="parallax-wrap">
                <div class="image overlay" data-type="background" data-speed="0">
                    <div class="stuff pri-pad">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-md-offset-2 col-sm-offset-2 text-center">
                                    <div class="main-title full text-center">
                                        <h2>Join our mailing list</h2>
                                        <p>
                                            Would you like to receive mailings from Private Wealth about new products, special offers and events? Please leave us with your email
                                            to be included on our list.
                                        </p>
                                    </div>
                                    <!--title-->

                                    <form action="#" class="form-inline">
                                        <div class="form-group email">
                                            <input type="text" placeholder="enter your email address">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit">
                                                Subscribe <i class="fa fa-long-arrow-right"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--secbscription-->
    </main>

@endsection


