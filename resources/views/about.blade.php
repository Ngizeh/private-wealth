@extends('layouts.app')

@section('content')

  <main class="pri-pad page-default">  
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-12"> 
                    <div class="sec-title icon-wrap">
                       <h3>Private wealth real estate </h3>
                   </div>
                    <h5>About Us</h5>
                     <p>
                        Private Wealth â€“ Real Estate Division offers various services leveraging on our experience, expertise and track record in the local Real Estate Market. We serve both institutional and Individual Investors seeking management and execution capacity to fulfill their Real Estate investment objectives.
                    </p>
                    
                     <div class="sec-title icon-wrap">
                       <h3>Services offered by PWRE </h3>
                   </div>
                      <h5>Property Management</h5>
                    <p>
                        Private Wealth through our Real Estate Division manages property for Corporate and Individual Investors in excess of Ksh. 1 billion.
                    </p>
                    
                    <p>
                        Once our development partners complete a development, our property management team steps in and assume the ongoing responsibilities for its operations. We currently provide property management services to virtually all of our residential, mixed-use, retail and commercial properties, including apartments, commercial, retail and mixed-use space.
                    </p>

                    <p>
                         Managing our group properties is an important part of our overall strategy and enables us to create additional value for our assets and that of our clients, over time. We maintain our properties with exceptional detail and undertake physical improvements on an ongoing basis
                    </p>
                    
                    <p>
                        For our commercial developments, we also devise and maintain an optimal retail tenant mix to ensure the propertyâ€™s continued appeal. These practices allow us to position each of our properties as best-in-class and help to increase their revenue streams.
                    </p>
                    <p>
                        Our property management functions include maintenance, administration, accounting, customer service, marketing and leasing. These daily activities are closely monitored and controlled by our Real Estate Team, which is responsible for budgeting, financial and regulatory compliance and reporting for all our properties. Our approach of providing direct, comprehensive management services under strategic direction of our executive officers enables us to maximize cash flow while protecting the long-term value of each asset.
                    </p>
                    <p>
                        Since we began, we have been known for our professional, responsive building management and for our philosophy of providing the utmost respect to our residents, tenants, employees and investors. People who live in buildings under our management can readily attest that our residential management group strives to maintain the highest possible standards in maintenance, administrative efficiency and resident services. These qualities have been instrumental in establishing us as an emerging distinctive property manager across the entire economic spectrum.
                    </p>
                    
                    <h5>Property Sales</h5>
                    <p>
                       Our property management team has the capacity to execute sales of both large scale Commercial and Residential buildings. We place emphasis on our ability to execute large commercial property sales within a short turn around period. Through our expertise, experience and network in Residential property, we have the capacity to design and execute sales and marketing plans to ensure successful sale of a large number of units to Institutional and Retail Investors..
                    </p>
                    
                    <h5>Property Letting</h5>
                    <p>
                        Our real estate team has developed a capacity for letting of property under our management with minimum turn around and vacancy rate. Our team has also developed a large pool f tenants whom we provide space as soon as vacancies emerge, minimizing our vacancies period. 
                    </p> 
                </div>
            </div>
        </div> 
    </main>

@endsection
