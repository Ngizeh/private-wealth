@extends('layouts.app')

@section('content')

      <main class="pri-pad contact-block">
        <section class="top pri-pad-b">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 text-center lh-normal">
                        <h2 class="lh-normal">We are waiting to assit you...</h2>
                        <p>Simply call, emailor use the form below to get in touch</p>
                    </div>
                </div>
            </div>
        </section>
        <!--top-->

        <div class="map-hold  pri-pad-b">
            <div class="overlay">
                <img src="/assets/images/spring-valley.png" style="border:0" allowfullscreen alt="Spring Valley">
           </div>
        </div>

        <!--map-->

        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7 contact-left left-block">
                    <div class="content-wrap mb-50">
                        <h3 class="lh-normal">Get in touch</h3>
                        <p>
                            Feel free to get in touch with us in all matter pertaining real estate, buying, selling and management .
                        </p>
                    </div>
                    <!--top-->

                    <div class="info-box">
                        <div>
                            <span class="fa fa-map-marker"></span>
                            <h5 class="mb-10">Official Address</h5>
                            <p>
                                Spring Valley,Business Park, Nairobi, Kenya
                            </p>
                        </div>
                        <!--address-->

                        <div>
                            <span class="fa fa-phone"></span>
                            <h5 class="mb-10">Phone Number</h5>
                            <p>
                               +254-715-008-946
                            </p>
                        </div>
                        <!--address-->

                        <div>
                            <span class="fa fa-envelope"></span>
                            <h5 class="mb-10">Email Address</h5>
                            <p>
                                <a href="#">info@privatewealth.com</a>
                            </p>
                        </div>
                        <!--address-->
                    </div>
                </div>
                <!--left block-->

                <div class="col-md-5 col-sm-5 contact-form">
                    <div class="box box-shadow">
                        <div class="box-title">
                            <h4 class="lh-normal text-white mb-0">Say something for us...</h4>
                        </div>
                        <!--title-->

                        <form action="#">
                            <div class="form-group">
                                <label>How would you like us to call you?</label>
                                <input type="text" placeholder="Please write your name here">
                            </div>
                            <!--name-->

                            <div class="form-group">
                                <label>Which inbox should we reply on?</label>
                                <input type="text" placeholder="Enter your email address here">
                            </div>
                            <!--email-->

                            <div class="form-group">
                                <label>How can we help you?</label>
                                <textarea placeholder="Please write your message here"></textarea>
                            </div>
                            <!--message-->

                            <div class="form-group">
                                <button type="submit">
                                    Send Message <i class="fa fa-long-arrow-right"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--contact form-->
            </div>
        </div>
    </main>

@endsection
