
require('./bootstrap');

window.Vue = require('vue');

Vue.component('intro-section', require('./components/IntroSection.vue'));

const app = new Vue({ el : '#app'})