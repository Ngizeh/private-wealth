<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
           'name' => "Ngizeh",
            'email' => "ngizeh@gmail.com",
            'password' => Hash::make('123456'),
            'remember_token' => null,
        ]);
           DB::table('users')->insert([
           'name' => "Private Wealth",
            'email' => "info@privatewealth.co.ke",
            'password' => Hash::make('private123456!'),
            'remember_token' => null,
        ]);
    }
}
