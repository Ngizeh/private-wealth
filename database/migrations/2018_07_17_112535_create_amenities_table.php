<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amenities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('property_id')->index();
            $table->boolean('gym')->default(0)->nullable();
            $table->boolean('swimming')->default(0)->nullable();
            $table->boolean('cctv')->default(0)->nullable();
            $table->boolean('scenic_view')->default(0)->nullable();
            $table->boolean('solar_panel')->default(0)->nullable();
            $table->boolean('bio_digestor')->default(0)->nullable();
            $table->boolean('underground')->default(0)->nullable();
            $table->boolean('internet')->default(0)->nullable();
            $table->boolean('lift_elevator')->default(0)->nullable();
            $table->boolean('pets_allowed')->default(0)->nullable();
            $table->boolean('balcony')->default(0)->nullable();
            $table->boolean('alarm_backup')->default(0)->nullable();
            $table->boolean('wifi')->default(0)->nullable();
            $table->boolean('bore_hole')->default(0)->nullable();
            $table->boolean('electric_fence')->default(0)->nullable();
            $table->boolean('garden')->default(0)->nullable();
            $table->boolean('build_in_cupboard')->default(0)->nullable();
            $table->boolean('ensuite')->default(0)->nullable();
            $table->boolean('water_tank')->default(0)->nullable();
            $table->boolean('walkin_closet')->default(0)->nullable();
            
            $table->foreign('property_id')->references('id')
                ->on('properties')->onDelete('cascade');
                
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amenities');
    }
}
