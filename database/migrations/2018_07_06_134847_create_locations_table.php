<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('property_id')->index();
            $table->foreign('property_id')->references('id')
                ->on('properties')->onDelete('cascade');
            $table->string('address')->nullable();
            $table->string('state')->nullable();
            $table->string('county')->nullable();   
            $table->string('zip_code')->nullable();
            $table->double('lat',20, 10);
            $table->double('lng',20, 10);;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
