<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandAmenitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('land_amenities', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('land_id')->index();
            $table->boolean('fencing')->default(0)->nullable();
            $table->boolean('water')->default(0)->nullable();
            $table->boolean('electricity')->default(0)->nullable();
            $table->boolean('access_road')->default(0)->nullable();
            $table->boolean('existing_building')->default(0)->nullable();
            
            $table->foreign('land_id')->references('id')
                ->on('lands')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('land_amenities');
    }
}
