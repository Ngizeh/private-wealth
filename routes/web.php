<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::view('about', 'about');

Route::view('contact', 'contact');

Route::view('/admin','admin.index')->middleware('auth');

Route::get('land-amenities/{slug}', 'LandAmenityController@create');
Route::get('land-amenities/{slug}/edit', 'LandAmenityController@edit');
Route::post('lands-amenity/{slug}', 'LandAmenityController@store');
Route::patch('lands-amenities/{slug}', 'LandAmenityController@update');

Route::get('lands-property', 'LandsController@index');
Route::post('lands-property', 'LandsController@store');
Route::get('lands-property/create', 'LandsController@create');
Route::get('lands-property/{slug}', 'LandsController@show');
Route::get('lands-property/{slug}/edit', 'LandsController@edit');
Route::patch('lands-property/{slug}', 'LandsController@update');
Route::delete('lands-property/{slug}', 'LandsController@destroy');


Route::get('user-dashboard', 'UserController@index');
Route::get('user-settings', 'UserController@show');
Route::get('user-land', 'UserController@lands');
Route::patch('user-settings/{user}', 'UserController@update');

Route::get('property', 'PropertyController@index');
Route::post('property', 'PropertyController@store');
Route::get('property/create', 'PropertyController@create');
Route::get('property/{slug}', 'PropertyController@show');
Route::get('property/{slug}/edit', 'PropertyController@edit');
Route::patch('property/{slug}', 'PropertyController@update');
Route::delete('property/{slug}', 'PropertyController@destroy');
Route::get('preview/{slug}', 'PreviewController@show');
Route::patch('preview/{slug}', 'PreviewController@update');

Route::get('amenity/{slug}', 'AmenityController@create');
Route::post('amenities/{slug}', 'AmenityController@store');
Route::get('amenity/{slug}/edit', 'AmenityController@edit');
Route::patch('amenities/{slug}', 'AmenityController@update');


Route::get('location/{slug}', 'LocationController@create');
Route::get('location/{slug}/edit', 'LocationController@edit');
Route::post('locations/{slug}', 'LocationController@store');
Route::patch('location/update', 'LocationController@update');


Route::get('media/{slug}', 'MediaController@create');
Route::post('medias/{slug}', 'MediaController@store');
Route::delete('medias/{id}', 'MediaController@destroy');

Route::resource('media', 'MediaController');


Route::resource('agent', 'AgentDetailController');
Route::get('rentals', 'RentalController@index');
Route::get('sale', 'SalesController@index');
Route::get('commercials', 'CommercialsController@index');
Route::get('residentials', 'ResidentialsController@index');



Route::get('land-media/{slug}', 'LandMediaController@create');
Route::post('land-medias/{slug}', 'LandMediaController@store');
Route::get('land-medias/{slug}/edit', 'LandMediaController@edit');
Route::delete('land-medias/{slug}', 'LandMediaController@destroy');

Route::get('land-preview/{slug}', 'LandPreview@show');
Route::patch('land-preview/{slug}', 'LandPreview@update');

Auth::routes();

Route::get('/search', 'SearchController@index');

Route::get('/', 'HomeController@index');
